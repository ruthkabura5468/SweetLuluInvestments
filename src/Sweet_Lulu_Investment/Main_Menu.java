/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sweet_Lulu_Investment;

//import AppPackage.AnimationClass;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Ruth
 */
public class Main_Menu extends javax.swing.JFrame {
//    AnimationClass AC = new AnimationClass ();
    int [] array = new int [6]; 
    private double num;
    private int calculation;
    private double ans;
    static int grand;
    static int ttl;
    getSum sum = new getSum();
        PreparedStatement ps = null;
//        Statement stmt;
        ResultSet rs = null;
        String AccessType;
        LogIn log = new LogIn();
        JTable1 table = new JTable1();
        JTable2 table1 = new JTable2();
    private DefaultTableModel model;
    String admin = "SELECT `Admin_Name`, `Admin_Password` FROM `administrator`";
    String user = "SELECT `First_Name`, `Password` FROM `user`";
    String select = "SELECT Date, Expense, Amount, Other_Information FROM expenses WHERE Expense LIKE ?";
    String report = "SELECT `Purchase_Date`, `Name`, `Category`, `Unit`, `Quantity`, `Selling_Price`, `Total` FROM sales WHERE Name LIKE ?";
    String menu = "SELECT * FROM menu_category WHERE Category LIKE ?";
    String item = "SELECT Name, Category, Unit_of_Measurement, Price FROM item_manager WHERE Name LIKE ?";
    String cust = "SELECT Customer, Contact, Address FROM customers WHERE Customer LIKE ?";
    String items = "SELECT Name, Category, Unit_of_Measurement FROM item_manager WHERE Name LIKE ?";
    String order = "SELECT Order_ID, Date, Customer, Name, Unit_of_measurement, Quantity, Total FROM orders WHERE Customer LIKE ?";
    String due = "SELECT Order_ID, Date, Customer, Name, Unit_of_measurement, Quantity, Total FROM orders WHERE Delivery_Date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 1 DAY) AND Customer LIKE ?";
    String ttle = "SELECT `Customer`, `Contact`, `Grand_Total` FROM `total_balance` WHERE Customer LIKE ?";
    String sql = "SELECT Date, Expense, Amount, Other_Information FROM expenses WHERE STR_TO_DATE(Date, '%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?, '%Y-%m-%d')";
    String sql1 = "SELECT `Purchase_Date`, `Name`, `Category`, `Unit`, `Quantity`, `Selling_Price`, `Total` FROM sales WHERE STR_TO_DATE(Purchase_Date, '%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?, '%Y-%m-%d')";
    
   
    public Connection getConnection(){
                    Connection con = null;
    try {
      con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sweet_lulu", "root", "");
      return con;
    } catch(SQLException ex){
    JOptionPane.showMessageDialog(null, ex);
    return null;
    }
    }
    /**
     * Creates new form Main_Menu
     */
    public Main_Menu() {
        this.setExtendedState(this.getExtendedState() | Main_Menu.MAXIMIZED_BOTH);
        initComponents();
        txtcurr.setEchoChar('*');
            txtnew.setEchoChar('*');
            txtconfirm.setEchoChar('*');
    }
    
    public Main_Menu(String AccessType) {
        this.setExtendedState(this.getExtendedState() | Main_Menu.MAXIMIZED_BOTH);
        initComponents();
        txtPDate4.setVisible(false);
        txtPDate2.setVisible(false);
        jDateChooser9.setVisible(false);
         Date date = new Date();
        jDateChooser9.setDate(date);
        txtcurr.setEchoChar('*');
            txtnew.setEchoChar('*');
            txtconfirm.setEchoChar('*');
         lblUser1.setText(AccessType);
         FilterTable5(jTable5, sql, jDateChooser9.getDate(), jDateChooser9.getDate());
         FilterTable5(jTable6, sql1, jDateChooser9.getDate(), jDateChooser9.getDate());
         table1.FillTable(jTable3, user);
         table1.FillTable(jTable4, admin);
         table.FillTable(jTable1, menu, jTextField6);
          table.FillTable(jTable2, item, jTextField5);
           table.FillTable(jTable11, order, jTextField9);
            table.FillTable(jTable13, due, jTextField10);
            table.FillTable(jTable14, ttle, jTextField12);
            Date ();
          fillCombo ();
          clearSale1 ();
          clearSale ();
         setButtons ();
         showDate();
        showTime();
    }
    
    public void setButtons () {
    if ("Employee".equals(log.AccessType))
        {
            btnRegister.setVisible(false);
            btnMenus.setVisible(true);
            jDateChooser6.setEnabled(false);
            jDateChooser7.setEnabled(false);
            jDateChooser8.setEnabled(false);
        }
    }
    
    public void Date() {
    Date date = new Date();
        jDateChooser6.setDate(date);
        jDateChooser7.setDate(date);
        jDateChooser8.setDate(date);
    }
        
      // Display the current date
       public void showDate() {
        Date d = new Date();
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
     lblDate.setText(s.format(d)); 
    }
        
        // Display the current time
    public void showTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date d = new Date();
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
            lblTime.setText(s.format(d));
            }
        }).start();
    }
       
    // Calculations for the calculator
    public void arithmetic_operation() {
    switch (calculation) {
        case 1: // Addition
            ans = num + Double.parseDouble(jTextField2.getText());
            jTextField2.setText(Double.toString(ans));
            break;
            
        case 2: // Subtraction
            ans = num - Double.parseDouble(jTextField2.getText());
            jTextField2.setText(Double.toString(ans));
            break;
            
        case 3: // Multiplication
            ans = num * Double.parseDouble(jTextField2.getText());
            jTextField2.setText(Double.toString(ans));
            break;
            
        case 4: // Division
            ans = num / Double.parseDouble(jTextField2.getText());
            jTextField2.setText(Double.toString(ans));
            break;
    }
    }
    
    public void Del () {
     try{
            Connection con = getConnection();
            String sql = "select * FROM user";
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    // Change password
        public void ChangePass(){
        // Changing the current password
                 Connection con = getConnection();
        try {
            if (log.AccessType == "Administrator") {
                String query = "update administrator set Admin_Password = ? where Admin_Name = ?";
                PreparedStatement preparedStmt = con.prepareStatement(query);
                String Pass = String.valueOf(txtnew.getPassword());
                String ConPass = String.valueOf(txtconfirm.getPassword());
                if (txtconfirm.getText().length() > 0) {
                    if (Pass == null ? ConPass == null : Pass.equals(ConPass)) {
                        preparedStmt.setString(1, Pass);
                        preparedStmt.setString(2, log.name);

                        // execute the java preparedstatement
                        preparedStmt.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Password Update Successful");
                         txtcurr.setText(null);
                        txtnew.setText(null);
                        txtconfirm.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "Passwords do not Match");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Empty Fields Detected!");
                }
            }

            if (log.AccessType == "Employee") {
                String query = "update user set Password = ? where First_Name = ?";
                PreparedStatement preparedStmt = con.prepareStatement(query);
                String Pass = String.valueOf(txtnew.getPassword());
                String ConPass = String.valueOf(txtconfirm.getPassword());
                if (txtconfirm.getText().length() > 0) {
                    if (Pass == null ? ConPass == null : Pass.equals(ConPass)) {
                        preparedStmt.setString(1, Pass);
                        preparedStmt.setString(2, log.name);

                        // execute the java preparedstatement
                        preparedStmt.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Password Update Successful");
                         txtcurr.setText(null);
                        txtnew.setText(null);
                        txtconfirm.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "Passwords do not match");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Empty Fields Detected!");
                }
            }
        } catch (SQLException ex) {
            // JOptionPane.showMessageDialog(this, ex.getMessage());
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
    if (rs != null) {
        try {
            rs.close();
        } catch (SQLException e) { /* ignored */}
    }
    if (ps != null) {
        try {
            ps.close();
        } catch (SQLException e) { /* ignored */}
    }
    if (con != null) {
        try {
            con.close();
        } catch (SQLException e) { /* ignored */}
    }
}
    }
              
    public void FilterTable5(JTable table, String query, Date date01, Date date02) {
    Connection con = getConnection();
    try {
    ps = con.prepareStatement(query);
    java.sql.Timestamp date1 = new java.sql.Timestamp(date01.getTime());
    java.sql.Timestamp date2 = new java.sql.Timestamp(date02.getTime());
    ps.setTimestamp(2, date2);
    ps.setTimestamp(1, date1);
    rs = ps.executeQuery();
     //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = rs.getMetaData().getColumnCount();
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
        }
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null, ex);
    } finally {
        try {
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }
    
    // Sum of the rows in expenses table
    public int getTotal() {
    int sum = 0;
    for (int i = 0; i < jTable5.getRowCount(); i ++) {
        sum = sum + Integer.parseInt(jTable5.getValueAt(i, 2).toString()); 
    }
    return sum;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Change_Pass = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        lblUser5 = new javax.swing.JLabel();
        txtcurr = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtnew = new javax.swing.JPasswordField();
        txtconfirm = new javax.swing.JPasswordField();
        jLabel8 = new javax.swing.JLabel();
        btnUpdatePass = new javax.swing.JButton();
        jCheckBox4 = new javax.swing.JCheckBox();
        Accounts = new javax.swing.JDialog();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel26 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        lblFName2 = new javax.swing.JLabel();
        txtFName2 = new javax.swing.JTextField();
        lblPass3 = new javax.swing.JLabel();
        btnNew1 = new javax.swing.JButton();
        btnSave5 = new javax.swing.JButton();
        btnDel3 = new javax.swing.JButton();
        txtFName5 = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        lblFName3 = new javax.swing.JLabel();
        txtFName3 = new javax.swing.JTextField();
        lblPass4 = new javax.swing.JLabel();
        btnDel4 = new javax.swing.JButton();
        btnSave6 = new javax.swing.JButton();
        btnNew2 = new javax.swing.JButton();
        txtFName4 = new javax.swing.JTextField();
        Expenses = new javax.swing.JDialog();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        btnSave2 = new javax.swing.JButton();
        btnDelete1 = new javax.swing.JButton();
        btnCancel1 = new javax.swing.JButton();
        lblTotal1 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        txtInfo = new javax.swing.JTextArea();
        jLabel41 = new javax.swing.JLabel();
        txtAmnt = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtExpense = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton30 = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel44 = new javax.swing.JLabel();
        lblUser10 = new javax.swing.JLabel();
        jDateChooser8 = new com.toedter.calendar.JDateChooser();
        Report = new javax.swing.JDialog();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jLabel49 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jDateChooser4 = new com.toedter.calendar.JDateChooser();
        jLabel48 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton31 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane15 = new javax.swing.JScrollPane();
        jTable14 = new javax.swing.JTable();
        jTextField12 = new javax.swing.JTextField();
        jLabel92 = new javax.swing.JLabel();
        txtName4 = new javax.swing.JTextField();
        jLabel93 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        txtContact3 = new javax.swing.JTextField();
        jLabel95 = new javax.swing.JLabel();
        txttotal3 = new javax.swing.JTextField();
        btnRemove8 = new javax.swing.JButton();
        txttotal4 = new javax.swing.JTextField();
        jLabel96 = new javax.swing.JLabel();
        Calculator = new javax.swing.JDialog();
        jPanel7 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        jButton27 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton28 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jTextField2 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JTextField();
        Menus = new javax.swing.JDialog();
        jPanel8 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtUnit = new javax.swing.JTextField();
        txtCat = new javax.swing.JComboBox<>();
        btnDel1 = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnSave3 = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        btnSave4 = new javax.swing.JButton();
        btnDel2 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        txtCat1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtID1 = new javax.swing.JTextField();
        Order = new javax.swing.JDialog();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtname = new javax.swing.JTextField();
        txtcat = new javax.swing.JTextField();
        txtunit = new javax.swing.JTextField();
        txttotal = new javax.swing.JTextField();
        txtqty = new javax.swing.JTextField();
        btnSearch2 = new javax.swing.JButton();
        txtPDate4 = new javax.swing.JLabel();
        jDateChooser7 = new com.toedter.calendar.JDateChooser();
        jPanel14 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtName1 = new javax.swing.JTextField();
        txtContact = new javax.swing.JTextField();
        btnSearch1 = new javax.swing.JButton();
        txtAddress = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel56 = new javax.swing.JLabel();
        txtPAmnt1 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jDateChooser5 = new com.toedter.calendar.JDateChooser();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable8 = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnUpdateItem = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnProcess = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTable11 = new javax.swing.JTable();
        jLabel62 = new javax.swing.JLabel();
        txtID = new javax.swing.JLabel();
        txtname2 = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        txtcat2 = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        txtunit2 = new javax.swing.JTextField();
        jLabel68 = new javax.swing.JLabel();
        txtqty2 = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();
        txtName2 = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        txtContact1 = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        txtAddress1 = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        btnRemove6 = new javax.swing.JButton();
        txtPDate5 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel82 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTable13 = new javax.swing.JTable();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        txtqty3 = new javax.swing.JTextField();
        txtunit3 = new javax.swing.JTextField();
        jLabel75 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        txtcat3 = new javax.swing.JTextField();
        txtname3 = new javax.swing.JTextField();
        jLabel77 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        txtAddress2 = new javax.swing.JTextField();
        txtContact2 = new javax.swing.JTextField();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        txtName3 = new javax.swing.JTextField();
        txtPDate3 = new javax.swing.JLabel();
        btnRemove5 = new javax.swing.JButton();
        jLabel83 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel86 = new javax.swing.JLabel();
        txtID2 = new javax.swing.JLabel();
        NewSale = new javax.swing.JDialog();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txtname1 = new javax.swing.JTextField();
        txtcat1 = new javax.swing.JTextField();
        txtunit1 = new javax.swing.JTextField();
        txttotal1 = new javax.swing.JTextField();
        txtqty1 = new javax.swing.JTextField();
        btnSearch3 = new javax.swing.JButton();
        txtPDate2 = new javax.swing.JLabel();
        jDateChooser6 = new com.toedter.calendar.JDateChooser();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTable9 = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel57 = new javax.swing.JLabel();
        txtPAmnt2 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        btnAdd1 = new javax.swing.JButton();
        btnUpdateItem1 = new javax.swing.JButton();
        btnRemove1 = new javax.swing.JButton();
        btnProcess1 = new javax.swing.JButton();
        lblUser4 = new javax.swing.JLabel();
        OrderCustomer = new javax.swing.JDialog();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable7 = new javax.swing.JTable();
        jTextField7 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        SaleItems = new javax.swing.JDialog();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTable10 = new javax.swing.JTable();
        jTextField8 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        OrderItems = new javax.swing.JDialog();
        jPanel24 = new javax.swing.JPanel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTable12 = new javax.swing.JTable();
        jTextField11 = new javax.swing.JTextField();
        jLabel84 = new javax.swing.JLabel();
        jLabel85 = new javax.swing.JLabel();
        SalePayment = new javax.swing.JDialog();
        jPanel20 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        txtPAmnt = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        txtTendered = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        txtBal = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        btnPrint = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        OrderPayment = new javax.swing.JDialog();
        jPanel21 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        txtPAmnt3 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        txtTendered1 = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        txtBal1 = new javax.swing.JLabel();
        btnSubmit1 = new javax.swing.JButton();
        jLabel65 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        MakePayment = new javax.swing.JDialog();
        jPanel25 = new javax.swing.JPanel();
        jLabel87 = new javax.swing.JLabel();
        txtPAmnt4 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        txtTendered2 = new javax.swing.JTextField();
        jLabel89 = new javax.swing.JLabel();
        txtBal2 = new javax.swing.JLabel();
        btnSubmit2 = new javax.swing.JButton();
        jLabel90 = new javax.swing.JLabel();
        jLabel91 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        lblUser1 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblUser2 = new javax.swing.JLabel();
        btnNewSale = new javax.swing.JButton();
        btnExpense = new javax.swing.JButton();
        btnOrder = new javax.swing.JButton();
        btnPassword = new javax.swing.JButton();
        btnRegister = new javax.swing.JButton();
        btnMenus = new javax.swing.JButton();
        btnReport = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jDateChooser9 = new com.toedter.calendar.JDateChooser();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        Change_Pass.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Change_Pass.setLocation(new java.awt.Point(0, 0));
        Change_Pass.setMinimumSize(new java.awt.Dimension(633, 425));
        Change_Pass.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        Change_Pass.setResizable(false);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblUser5.setFont(new java.awt.Font("Lucida Calligraphy", 0, 30)); // NOI18N
        lblUser5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUser5.setText("Change Password");
        jPanel2.add(lblUser5, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 320, 50));

        txtcurr.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel2.add(txtcurr, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 240, 30));

        jLabel6.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jLabel6.setText("Current Password");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 140, -1));

        jLabel7.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jLabel7.setText("New Password");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 130, -1));

        txtnew.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel2.add(txtnew, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 180, 240, 30));

        txtconfirm.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel2.add(txtconfirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 250, 240, 30));

        jLabel8.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jLabel8.setText("Confirm Password");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 250, 140, -1));

        btnUpdatePass.setFont(new java.awt.Font("Corbel", 0, 20)); // NOI18N
        btnUpdatePass.setText("Update Password");
        btnUpdatePass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePassActionPerformed(evt);
            }
        });
        jPanel2.add(btnUpdatePass, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 320, 190, -1));

        jCheckBox4.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jCheckBox4.setHideActionText(true);
        jCheckBox4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jCheckBox4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/eye-eyelid-pupil_318-33238.jpg"))); // NOI18N
        jCheckBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox4ActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBox4, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 250, 20, 19));

        javax.swing.GroupLayout Change_PassLayout = new javax.swing.GroupLayout(Change_Pass.getContentPane());
        Change_Pass.getContentPane().setLayout(Change_PassLayout);
        Change_PassLayout.setHorizontalGroup(
            Change_PassLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Change_PassLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        Change_PassLayout.setVerticalGroup(
            Change_PassLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Accounts.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Accounts.setMinimumSize(new java.awt.Dimension(757, 529));
        Accounts.setModal(true);
        Accounts.setResizable(false);

        jTabbedPane2.setFont(new java.awt.Font("Lucida Calligraphy", 0, 18)); // NOI18N

        jPanel26.setLayout(null);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Name", "Password"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.getTableHeader().setReorderingAllowed(false);
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel26.add(jScrollPane3);
        jScrollPane3.setBounds(380, 30, 340, 402);

        lblFName2.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        lblFName2.setText("User Name");
        jPanel26.add(lblFName2);
        lblFName2.setBounds(20, 60, 80, 30);

        txtFName2.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel26.add(txtFName2);
        txtFName2.setBounds(110, 60, 220, 30);

        lblPass3.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        lblPass3.setText("Password");
        jPanel26.add(lblPass3);
        lblPass3.setBounds(20, 120, 80, 30);

        btnNew1.setText("New");
        btnNew1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNew1ActionPerformed(evt);
            }
        });
        jPanel26.add(btnNew1);
        btnNew1.setBounds(30, 210, 70, 50);

        btnSave5.setText("Save");
        btnSave5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave5ActionPerformed(evt);
            }
        });
        jPanel26.add(btnSave5);
        btnSave5.setBounds(150, 210, 70, 50);

        btnDel3.setText("Delete");
        btnDel3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel3ActionPerformed(evt);
            }
        });
        jPanel26.add(btnDel3);
        btnDel3.setBounds(280, 210, 70, 50);

        txtFName5.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel26.add(txtFName5);
        txtFName5.setBounds(110, 120, 220, 30);

        jTabbedPane2.addTab("Manage Users", jPanel26);

        jPanel11.setLayout(null);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User Name", "Password"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable4.getTableHeader().setReorderingAllowed(false);
        jTable4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable4MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jTable4);
        if (jTable4.getColumnModel().getColumnCount() > 0) {
            jTable4.getColumnModel().getColumn(0).setResizable(false);
            jTable4.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel11.add(jScrollPane4);
        jScrollPane4.setBounds(380, 30, 340, 402);

        lblFName3.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        lblFName3.setText("User Name");
        jPanel11.add(lblFName3);
        lblFName3.setBounds(20, 60, 80, 30);

        txtFName3.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel11.add(txtFName3);
        txtFName3.setBounds(110, 60, 220, 30);

        lblPass4.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        lblPass4.setText("Password");
        jPanel11.add(lblPass4);
        lblPass4.setBounds(20, 120, 80, 30);

        btnDel4.setText("Delete");
        btnDel4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel4ActionPerformed(evt);
            }
        });
        jPanel11.add(btnDel4);
        btnDel4.setBounds(280, 210, 70, 50);

        btnSave6.setText("Save");
        btnSave6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave6ActionPerformed(evt);
            }
        });
        jPanel11.add(btnSave6);
        btnSave6.setBounds(150, 210, 70, 50);

        btnNew2.setText("New");
        btnNew2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNew2ActionPerformed(evt);
            }
        });
        jPanel11.add(btnNew2);
        btnNew2.setBounds(30, 210, 70, 50);

        txtFName4.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        jPanel11.add(txtFName4);
        txtFName4.setBounds(110, 120, 220, 30);

        jTabbedPane2.addTab("Manage Admins", jPanel11);

        javax.swing.GroupLayout AccountsLayout = new javax.swing.GroupLayout(Accounts.getContentPane());
        Accounts.getContentPane().setLayout(AccountsLayout);
        AccountsLayout.setHorizontalGroup(
            AccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 757, Short.MAX_VALUE)
        );
        AccountsLayout.setVerticalGroup(
            AccountsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 529, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Expenses.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Expenses.setMinimumSize(new java.awt.Dimension(1144, 560));
        Expenses.setModal(true);
        Expenses.setResizable(false);

        jPanel5.setLayout(null);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Expense", "Amount", "Other Information"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable5.getTableHeader().setReorderingAllowed(false);
        jTable5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable5MouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(jTable5);
        if (jTable5.getColumnModel().getColumnCount() > 0) {
            jTable5.getColumnModel().getColumn(0).setResizable(false);
            jTable5.getColumnModel().getColumn(1).setResizable(false);
            jTable5.getColumnModel().getColumn(2).setResizable(false);
            jTable5.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel5.add(jScrollPane8);
        jScrollPane8.setBounds(10, 70, 650, 450);

        btnSave2.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnSave2.setText("Save");
        btnSave2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave2ActionPerformed(evt);
            }
        });
        jPanel5.add(btnSave2);
        btnSave2.setBounds(670, 480, 100, 30);

        btnDelete1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnDelete1.setText("Delete");
        btnDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelete1ActionPerformed(evt);
            }
        });
        jPanel5.add(btnDelete1);
        btnDelete1.setBounds(790, 480, 100, 30);

        btnCancel1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnCancel1.setText("Clear");
        btnCancel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel1ActionPerformed(evt);
            }
        });
        jPanel5.add(btnCancel1);
        btnCancel1.setBounds(910, 480, 100, 30);

        lblTotal1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.add(lblTotal1);
        lblTotal1.setBounds(830, 410, 200, 30);

        jLabel39.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel39.setText("Total Expenses");
        jPanel5.add(jLabel39);
        jLabel39.setBounds(680, 410, 120, 30);

        txtInfo.setColumns(10);
        txtInfo.setRows(3);
        txtInfo.setTabSize(5);
        jScrollPane10.setViewportView(txtInfo);

        jPanel5.add(jScrollPane10);
        jScrollPane10.setBounds(830, 320, 240, 70);

        jLabel41.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel41.setText("Other Information");
        jPanel5.add(jLabel41);
        jLabel41.setBounds(680, 320, 130, 30);

        txtAmnt.setText("0");
        txtAmnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmntActionPerformed(evt);
            }
        });
        jPanel5.add(txtAmnt);
        txtAmnt.setBounds(830, 280, 240, 30);

        jLabel40.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel40.setText("Amount");
        jPanel5.add(jLabel40);
        jLabel40.setBounds(680, 280, 80, 30);
        jPanel5.add(txtExpense);
        txtExpense.setBounds(830, 240, 240, 30);

        jLabel43.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel43.setText("Expense");
        jPanel5.add(jLabel43);
        jLabel43.setBounds(680, 240, 80, 30);

        jLabel42.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel42.setText("Date");
        jPanel5.add(jLabel42);
        jLabel42.setBounds(680, 200, 90, 30);

        jTextField3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField3CaretUpdate(evt);
            }
        });
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        jPanel5.add(jTextField3);
        jTextField3.setBounds(690, 120, 200, 30);

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(0, 0, 204));
        jLabel37.setText("* Search by Expense");
        jPanel5.add(jLabel37);
        jLabel37.setBounds(900, 120, 200, 30);

        jDateChooser1.setDateFormatString("dd-MM-yyyy");
        jPanel5.add(jDateChooser1);
        jDateChooser1.setBounds(880, 70, 150, 30);

        jButton30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/filter.png"))); // NOI18N
        jButton30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton30ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton30);
        jButton30.setBounds(1050, 70, 30, 30);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel45.setText("to");
        jPanel5.add(jLabel45);
        jLabel45.setBounds(850, 70, 30, 30);

        jDateChooser2.setDateFormatString("dd-MM-yyyy");
        jPanel5.add(jDateChooser2);
        jDateChooser2.setBounds(700, 70, 150, 30);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("From");
        jPanel5.add(jLabel44);
        jLabel44.setBounds(660, 70, 40, 30);

        lblUser10.setFont(new java.awt.Font("Lucida Calligraphy", 0, 30)); // NOI18N
        lblUser10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUser10.setText("Expenses");
        jPanel5.add(lblUser10);
        lblUser10.setBounds(430, 0, 190, 50);
        jPanel5.add(jDateChooser8);
        jDateChooser8.setBounds(830, 200, 240, 30);

        javax.swing.GroupLayout ExpensesLayout = new javax.swing.GroupLayout(Expenses.getContentPane());
        Expenses.getContentPane().setLayout(ExpensesLayout);
        ExpensesLayout.setHorizontalGroup(
            ExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ExpensesLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 1144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        ExpensesLayout.setVerticalGroup(
            ExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Report.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Report.setMinimumSize(new java.awt.Dimension(1144, 560));
        Report.setModal(true);
        Report.setResizable(false);

        jTabbedPane4.setFont(new java.awt.Font("Lucida Calligraphy", 0, 18)); // NOI18N

        jPanel6.setLayout(null);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Name", "Category", "Unit", "Quantity", "Selling Price", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable6.getTableHeader().setReorderingAllowed(false);
        jScrollPane9.setViewportView(jTable6);

        jPanel6.add(jScrollPane9);
        jScrollPane9.setBounds(20, 20, 660, 440);

        jLabel49.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
        jLabel49.setText("Total Sales Amount");
        jPanel6.add(jLabel49);
        jLabel49.setBounds(700, 170, 150, 30);

        lblTotal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel6.add(lblTotal);
        lblTotal.setBounds(860, 170, 200, 30);

        jTextField4.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField4CaretUpdate(evt);
            }
        });
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jPanel6.add(jTextField4);
        jTextField4.setBounds(710, 100, 200, 30);

        jDateChooser4.setDateFormatString("dd-MM-yyyy");
        jPanel6.add(jDateChooser4);
        jDateChooser4.setBounds(720, 50, 150, 30);

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("to");
        jPanel6.add(jLabel48);
        jLabel48.setBounds(870, 50, 30, 30);

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("From");
        jPanel6.add(jLabel47);
        jLabel47.setBounds(680, 50, 40, 30);

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(0, 0, 204));
        jLabel46.setText("* Search by Item Name");
        jPanel6.add(jLabel46);
        jLabel46.setBounds(920, 100, 190, 30);

        jDateChooser3.setDateFormatString("dd-MM-yyyy");
        jPanel6.add(jDateChooser3);
        jDateChooser3.setBounds(900, 50, 150, 30);

        jButton31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/filter.png"))); // NOI18N
        jButton31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton31ActionPerformed(evt);
            }
        });
        jPanel6.add(jButton31);
        jButton31.setBounds(1070, 50, 30, 30);

        jTabbedPane4.addTab("Sales Report", jPanel6);

        jPanel4.setLayout(null);

        jTable14.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer", "Contact", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable14.getTableHeader().setReorderingAllowed(false);
        jTable14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable14MouseClicked(evt);
            }
        });
        jScrollPane15.setViewportView(jTable14);
        if (jTable14.getColumnModel().getColumnCount() > 0) {
            jTable14.getColumnModel().getColumn(0).setResizable(false);
            jTable14.getColumnModel().getColumn(1).setResizable(false);
            jTable14.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel4.add(jScrollPane15);
        jScrollPane15.setBounds(50, 40, 500, 410);

        jTextField12.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField12CaretUpdate(evt);
            }
        });
        jPanel4.add(jTextField12);
        jTextField12.setBounds(610, 40, 200, 30);

        jLabel92.setForeground(java.awt.Color.blue);
        jLabel92.setText("* Search Customer");
        jPanel4.add(jLabel92);
        jLabel92.setBounds(820, 40, 130, 14);

        txtName4.setEditable(false);
        txtName4.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        txtName4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtName4ActionPerformed(evt);
            }
        });
        jPanel4.add(txtName4);
        txtName4.setBounds(710, 100, 240, 30);

        jLabel93.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel93.setText("Customer");
        jPanel4.add(jLabel93);
        jLabel93.setBounds(600, 100, 70, 30);

        jLabel94.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel94.setText("Contact");
        jPanel4.add(jLabel94);
        jLabel94.setBounds(600, 150, 70, 30);

        txtContact3.setEditable(false);
        txtContact3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtContact3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContact3ActionPerformed(evt);
            }
        });
        jPanel4.add(txtContact3);
        txtContact3.setBounds(710, 150, 240, 30);

        jLabel95.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel95.setText("Payment");
        jPanel4.add(jLabel95);
        jLabel95.setBounds(600, 280, 100, 30);

        txttotal3.setEditable(false);
        txttotal3.setText("0");
        jPanel4.add(txttotal3);
        txttotal3.setBounds(710, 200, 240, 30);

        btnRemove8.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnRemove8.setText("Make Payment");
        btnRemove8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemove8ActionPerformed(evt);
            }
        });
        jPanel4.add(btnRemove8);
        btnRemove8.setBounds(720, 360, 130, 50);

        txttotal4.setText("0");
        jPanel4.add(txttotal4);
        txttotal4.setBounds(710, 280, 240, 30);

        jLabel96.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel96.setText("Total Amount");
        jPanel4.add(jLabel96);
        jLabel96.setBounds(600, 200, 100, 30);

        jTabbedPane4.addTab("Order Invoice", jPanel4);

        javax.swing.GroupLayout ReportLayout = new javax.swing.GroupLayout(Report.getContentPane());
        Report.getContentPane().setLayout(ReportLayout);
        ReportLayout.setHorizontalGroup(
            ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReportLayout.createSequentialGroup()
                .addComponent(jTabbedPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        ReportLayout.setVerticalGroup(
            ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReportLayout.createSequentialGroup()
                .addComponent(jTabbedPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        Calculator.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Calculator.setMinimumSize(new java.awt.Dimension(287, 425));
        Calculator.setModal(true);
        Calculator.setResizable(false);

        jPanel7.setLayout(null);

        jButton1.setBackground(new java.awt.Color(250, 140, 42));
        jButton1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("=");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton1);
        jButton1.setBounds(210, 340, 70, 60);

        jButton26.setBackground(new java.awt.Color(250, 140, 42));
        jButton26.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton26.setForeground(new java.awt.Color(255, 255, 255));
        jButton26.setText("+");
        jButton26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton26ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton26);
        jButton26.setBounds(210, 280, 70, 60);

        jButton27.setBackground(new java.awt.Color(255, 255, 255));
        jButton27.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton27.setText(".");
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton27);
        jButton27.setBounds(140, 340, 70, 60);

        jButton23.setBackground(new java.awt.Color(255, 255, 255));
        jButton23.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton23.setText("0");
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton23);
        jButton23.setBounds(70, 340, 70, 60);

        jButton22.setBackground(new java.awt.Color(255, 255, 255));
        jButton22.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton22.setText("00");
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton22);
        jButton22.setBounds(0, 340, 70, 60);

        jButton21.setBackground(new java.awt.Color(255, 255, 255));
        jButton21.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton21.setText("1");
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton21);
        jButton21.setBounds(0, 280, 70, 60);

        jButton18.setBackground(new java.awt.Color(255, 255, 255));
        jButton18.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton18.setText("2");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton18);
        jButton18.setBounds(70, 280, 70, 60);

        jButton19.setBackground(new java.awt.Color(255, 255, 255));
        jButton19.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton19.setText("3");
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton19);
        jButton19.setBounds(140, 280, 70, 60);

        jButton25.setBackground(new java.awt.Color(250, 140, 42));
        jButton25.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton25.setForeground(new java.awt.Color(255, 255, 255));
        jButton25.setText("-");
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton25);
        jButton25.setBounds(210, 220, 70, 60);

        jButton24.setBackground(new java.awt.Color(250, 140, 42));
        jButton24.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton24.setForeground(new java.awt.Color(255, 255, 255));
        jButton24.setText("*");
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton24);
        jButton24.setBounds(210, 160, 70, 60);

        jButton16.setBackground(new java.awt.Color(255, 255, 255));
        jButton16.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton16.setText("6");
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton16);
        jButton16.setBounds(140, 220, 70, 60);

        jButton15.setBackground(new java.awt.Color(255, 255, 255));
        jButton15.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton15.setText("5");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton15);
        jButton15.setBounds(70, 220, 70, 60);

        jButton8.setBackground(new java.awt.Color(255, 255, 255));
        jButton8.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton8.setText("4");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton8);
        jButton8.setBounds(0, 220, 70, 60);

        jButton17.setBackground(new java.awt.Color(255, 255, 255));
        jButton17.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton17.setText("7");
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton17);
        jButton17.setBounds(0, 160, 70, 60);

        jButton20.setBackground(new java.awt.Color(255, 255, 255));
        jButton20.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton20.setText("8");
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton20);
        jButton20.setBounds(70, 160, 70, 60);

        jButton6.setBackground(new java.awt.Color(255, 255, 255));
        jButton6.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton6.setText("9");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton6);
        jButton6.setBounds(140, 160, 70, 60);

        jButton28.setBackground(new java.awt.Color(250, 140, 42));
        jButton28.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton28.setForeground(new java.awt.Color(255, 255, 255));
        jButton28.setText("/");
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton28);
        jButton28.setBounds(210, 100, 70, 60);

        jButton5.setBackground(new java.awt.Color(204, 204, 204));
        jButton5.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton5.setText("AC");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton5);
        jButton5.setBounds(0, 100, 70, 60);

        jButton4.setBackground(new java.awt.Color(204, 204, 204));
        jButton4.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton4.setText("+/-");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton4);
        jButton4.setBounds(140, 100, 70, 60);

        jButton3.setBackground(new java.awt.Color(204, 204, 204));
        jButton3.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        jButton3.setText("<<");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton3);
        jButton3.setBounds(70, 100, 70, 60);

        jTextField2.setEditable(false);
        jTextField2.setBackground(new java.awt.Color(46, 66, 99));
        jTextField2.setFont(new java.awt.Font("Arial", 0, 34)); // NOI18N
        jTextField2.setForeground(new java.awt.Color(255, 255, 255));
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel7.add(jTextField2);
        jTextField2.setBounds(0, 30, 280, 70);

        jLabel35.setEditable(false);
        jLabel35.setBackground(new java.awt.Color(46, 66, 99));
        jLabel35.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel7.add(jLabel35);
        jLabel35.setBounds(0, 0, 280, 30);

        javax.swing.GroupLayout CalculatorLayout = new javax.swing.GroupLayout(Calculator.getContentPane());
        Calculator.getContentPane().setLayout(CalculatorLayout);
        CalculatorLayout.setHorizontalGroup(
            CalculatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalculatorLayout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        CalculatorLayout.setVerticalGroup(
            CalculatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
        );

        Menus.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Menus.setMinimumSize(new java.awt.Dimension(759, 490));
        Menus.setModal(true);
        Menus.setResizable(false);

        jPanel8.setLayout(null);

        jTabbedPane1.setFont(new java.awt.Font("Lucida Calligraphy", 0, 20)); // NOI18N

        jPanel9.setLayout(null);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Category", "Units", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.getTableHeader().setReorderingAllowed(false);
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
            jTable2.getColumnModel().getColumn(2).setResizable(false);
            jTable2.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel9.add(jScrollPane2);
        jScrollPane2.setBounds(400, 30, 340, 360);

        jLabel13.setForeground(java.awt.Color.blue);
        jLabel13.setText("* Search Category");
        jPanel9.add(jLabel13);
        jLabel13.setBounds(230, 40, 100, 20);

        jTextField5.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField5CaretUpdate(evt);
            }
        });
        jPanel9.add(jTextField5);
        jTextField5.setBounds(30, 40, 190, 30);

        jLabel14.setText("Item");
        jPanel9.add(jLabel14);
        jLabel14.setBounds(30, 100, 40, 20);

        txtName.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel9.add(txtName);
        txtName.setBounds(30, 120, 210, 23);

        jLabel15.setText("Unit Price");
        jPanel9.add(jLabel15);
        jLabel15.setBounds(30, 250, 70, 20);

        txtPrice.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel9.add(txtPrice);
        txtPrice.setBounds(30, 270, 210, 23);

        jLabel16.setText("Category");
        jPanel9.add(jLabel16);
        jLabel16.setBounds(30, 150, 60, 20);

        jLabel17.setText("Unit of Measurement");
        jPanel9.add(jLabel17);
        jLabel17.setBounds(30, 200, 140, 20);

        txtUnit.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel9.add(txtUnit);
        txtUnit.setBounds(30, 220, 210, 23);

        txtCat.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel9.add(txtCat);
        txtCat.setBounds(30, 170, 210, 23);

        btnDel1.setText("Delete");
        btnDel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel1ActionPerformed(evt);
            }
        });
        jPanel9.add(btnDel1);
        btnDel1.setBounds(260, 330, 70, 50);

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        jPanel9.add(btnUpdate);
        btnUpdate.setBounds(180, 330, 70, 50);

        btnSave3.setText("Save");
        btnSave3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave3ActionPerformed(evt);
            }
        });
        jPanel9.add(btnSave3);
        btnSave3.setBounds(100, 330, 70, 50);

        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        jPanel9.add(btnNew);
        btnNew.setBounds(20, 330, 70, 50);

        jTabbedPane1.addTab("Item Manager", jPanel9);

        jPanel10.setLayout(null);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Category"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel10.add(jScrollPane1);
        jScrollPane1.setBounds(400, 30, 340, 360);

        jLabel11.setForeground(java.awt.Color.blue);
        jLabel11.setText("* Search Category");
        jPanel10.add(jLabel11);
        jLabel11.setBounds(230, 50, 100, 14);

        btnSave4.setText("Save");
        btnSave4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave4ActionPerformed(evt);
            }
        });
        jPanel10.add(btnSave4);
        btnSave4.setBounds(120, 280, 70, 50);

        btnDel2.setText("Delete");
        btnDel2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel2ActionPerformed(evt);
            }
        });
        jPanel10.add(btnDel2);
        btnDel2.setBounds(210, 280, 70, 50);

        jButton11.setText("New");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel10.add(jButton11);
        jButton11.setBounds(30, 280, 70, 50);

        txtCat1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel10.add(txtCat1);
        txtCat1.setBounds(40, 200, 200, 30);

        jLabel12.setText("Category");
        jPanel10.add(jLabel12);
        jLabel12.setBounds(40, 180, 70, 20);

        jTextField6.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField6CaretUpdate(evt);
            }
        });
        jPanel10.add(jTextField6);
        jTextField6.setBounds(40, 50, 180, 30);

        jLabel18.setText("ID");
        jPanel10.add(jLabel18);
        jLabel18.setBounds(40, 110, 70, 20);

        txtID1.setEditable(false);
        txtID1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel10.add(txtID1);
        txtID1.setBounds(40, 130, 200, 30);

        jTabbedPane1.addTab("Menu Categories", jPanel10);

        jPanel8.add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 0, 760, 490);

        javax.swing.GroupLayout MenusLayout = new javax.swing.GroupLayout(Menus.getContentPane());
        Menus.getContentPane().setLayout(MenusLayout);
        MenusLayout.setHorizontalGroup(
            MenusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenusLayout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 759, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        MenusLayout.setVerticalGroup(
            MenusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenusLayout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        Order.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Order.setMinimumSize(new java.awt.Dimension(933, 573));
        Order.setModal(true);
        Order.setResizable(false);

        jTabbedPane3.setFont(new java.awt.Font("Lucida Calligraphy", 0, 20)); // NOI18N

        jPanel3.setLayout(null);

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Item Manager", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Lucida Calligraphy", 0, 12))); // NOI18N
        jPanel13.setLayout(null);

        jLabel19.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel19.setText("Date");
        jPanel13.add(jLabel19);
        jLabel19.setBounds(30, 20, 60, 30);

        jLabel21.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel21.setText("Item");
        jPanel13.add(jLabel21);
        jLabel21.setBounds(30, 60, 60, 30);

        jLabel22.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel22.setText("Category");
        jPanel13.add(jLabel22);
        jLabel22.setBounds(30, 100, 90, 30);

        jLabel23.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel23.setText("Unit of Measure");
        jPanel13.add(jLabel23);
        jLabel23.setBounds(30, 140, 100, 30);

        jLabel24.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel24.setText("Quantity");
        jPanel13.add(jLabel24);
        jLabel24.setBounds(30, 180, 90, 30);

        jLabel25.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel25.setText("Unit Price");
        jPanel13.add(jLabel25);
        jLabel25.setBounds(30, 220, 70, 30);

        txtname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtnameCaretUpdate(evt);
            }
        });
        txtname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnameActionPerformed(evt);
            }
        });
        txtname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnameKeyReleased(evt);
            }
        });
        jPanel13.add(txtname);
        txtname.setBounds(140, 60, 210, 30);

        txtcat.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jPanel13.add(txtcat);
        txtcat.setBounds(140, 100, 240, 30);

        txtunit.setText("0");
        jPanel13.add(txtunit);
        txtunit.setBounds(140, 140, 240, 30);

        txttotal.setText("0");
        jPanel13.add(txttotal);
        txttotal.setBounds(140, 220, 240, 30);

        txtqty.setText("0");
        jPanel13.add(txtqty);
        txtqty.setBounds(140, 180, 240, 30);

        btnSearch2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/search.png"))); // NOI18N
        btnSearch2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch2ActionPerformed(evt);
            }
        });
        jPanel13.add(btnSearch2);
        btnSearch2.setBounds(350, 60, 32, 30);

        txtPDate4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel13.add(txtPDate4);
        txtPDate4.setBounds(100, 20, 30, 30);
        jPanel13.add(jDateChooser7);
        jDateChooser7.setBounds(140, 20, 240, 30);

        jPanel3.add(jPanel13);
        jPanel13.setBounds(40, 30, 410, 260);

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Customer Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Lucida Calligraphy", 0, 12))); // NOI18N
        jPanel14.setLayout(null);

        jLabel30.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel30.setText("Name");
        jPanel14.add(jLabel30);
        jLabel30.setBounds(20, 30, 70, 30);

        jLabel31.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel31.setText("Contact");
        jPanel14.add(jLabel31);
        jLabel31.setBounds(20, 80, 70, 30);

        jLabel32.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel32.setText("Address");
        jPanel14.add(jLabel32);
        jLabel32.setBounds(20, 127, 80, 30);

        txtName1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        txtName1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtName1ActionPerformed(evt);
            }
        });
        jPanel14.add(txtName1);
        txtName1.setBounds(100, 30, 210, 30);

        txtContact.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel14.add(txtContact);
        txtContact.setBounds(100, 80, 240, 30);

        btnSearch1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/search.png"))); // NOI18N
        btnSearch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch1ActionPerformed(evt);
            }
        });
        jPanel14.add(btnSearch1);
        btnSearch1.setBounds(310, 30, 32, 30);

        txtAddress.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel14.add(txtAddress);
        txtAddress.setBounds(100, 130, 240, 30);

        jPanel3.add(jPanel14);
        jPanel14.setBounds(480, 30, 380, 190);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Payment Manager", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Lucida Calligraphy", 0, 12))); // NOI18N
        jPanel15.setLayout(null);

        jLabel20.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel20.setText("Delivery Date");
        jPanel15.add(jLabel20);
        jLabel20.setBounds(20, 60, 110, 30);

        jComboBox1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "Lipa na M-Pesa", "Other Means" }));
        jPanel15.add(jComboBox1);
        jComboBox1.setBounds(150, 20, 190, 30);

        jLabel56.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("Amount Due");
        jPanel15.add(jLabel56);
        jLabel56.setBounds(120, 100, 100, 30);

        txtPAmnt1.setFont(new java.awt.Font("Cantarell", 0, 36)); // NOI18N
        txtPAmnt1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt1.setText("0");
        txtPAmnt1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel15.add(txtPAmnt1);
        txtPAmnt1.setBounds(50, 130, 260, 50);

        jLabel26.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel26.setText("Mode Of Payment");
        jPanel15.add(jLabel26);
        jLabel26.setBounds(20, 20, 130, 30);
        jPanel15.add(jDateChooser5);
        jDateChooser5.setBounds(150, 60, 190, 30);

        jPanel3.add(jPanel15);
        jPanel15.setBounds(480, 230, 380, 190);

        jTable8.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item", "Category", "Unit", "Quantity", "Total", "Time", "Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable8.getTableHeader().setReorderingAllowed(false);
        jTable8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable8MouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTable8);
        if (jTable8.getColumnModel().getColumnCount() > 0) {
            jTable8.getColumnModel().getColumn(0).setResizable(false);
            jTable8.getColumnModel().getColumn(1).setResizable(false);
            jTable8.getColumnModel().getColumn(2).setResizable(false);
            jTable8.getColumnModel().getColumn(3).setResizable(false);
            jTable8.getColumnModel().getColumn(4).setResizable(false);
            jTable8.getColumnModel().getColumn(5).setResizable(false);
            jTable8.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable8.getColumnModel().getColumn(6).setResizable(false);
            jTable8.getColumnModel().getColumn(6).setPreferredWidth(0);
        }

        jPanel3.add(jScrollPane6);
        jScrollPane6.setBounds(40, 300, 410, 180);

        btnAdd.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        jPanel3.add(btnAdd);
        btnAdd.setBounds(480, 430, 78, 50);

        btnUpdateItem.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnUpdateItem.setText("Update");
        btnUpdateItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateItemActionPerformed(evt);
            }
        });
        jPanel3.add(btnUpdateItem);
        btnUpdateItem.setBounds(580, 430, 78, 50);

        btnRemove.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnRemove.setText("Delete");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        jPanel3.add(btnRemove);
        btnRemove.setBounds(680, 430, 78, 50);

        btnProcess.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });
        jPanel3.add(btnProcess);
        btnProcess.setBounds(780, 430, 78, 50);

        jTabbedPane3.addTab("Make Order", jPanel3);

        jPanel23.setLayout(null);

        jTable11.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order ID", "Date", "Customer", "Item", "Unit", "Quantity", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable11.getTableHeader().setReorderingAllowed(false);
        jTable11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable11MouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(jTable11);
        if (jTable11.getColumnModel().getColumnCount() > 0) {
            jTable11.getColumnModel().getColumn(0).setResizable(false);
            jTable11.getColumnModel().getColumn(1).setResizable(false);
            jTable11.getColumnModel().getColumn(2).setResizable(false);
            jTable11.getColumnModel().getColumn(3).setResizable(false);
            jTable11.getColumnModel().getColumn(3).setHeaderValue("Item");
            jTable11.getColumnModel().getColumn(4).setResizable(false);
            jTable11.getColumnModel().getColumn(4).setHeaderValue("Unit");
            jTable11.getColumnModel().getColumn(5).setResizable(false);
            jTable11.getColumnModel().getColumn(5).setHeaderValue("Quantity");
            jTable11.getColumnModel().getColumn(6).setResizable(false);
            jTable11.getColumnModel().getColumn(6).setHeaderValue("Total");
        }

        jPanel23.add(jScrollPane12);
        jScrollPane12.setBounds(23, 80, 500, 400);

        jLabel62.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel62.setText("Order ID");
        jPanel23.add(jLabel62);
        jLabel62.setBounds(540, 40, 90, 30);

        txtID.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel23.add(txtID);
        txtID.setBounds(650, 40, 240, 30);

        txtname2.setEditable(false);
        txtname2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtname2CaretUpdate(evt);
            }
        });
        txtname2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtname2ActionPerformed(evt);
            }
        });
        txtname2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtname2KeyReleased(evt);
            }
        });
        jPanel23.add(txtname2);
        txtname2.setBounds(650, 200, 240, 30);

        jLabel63.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel63.setText("Name");
        jPanel23.add(jLabel63);
        jLabel63.setBounds(540, 200, 90, 30);

        jLabel66.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel66.setText("Category");
        jPanel23.add(jLabel66);
        jLabel66.setBounds(540, 240, 90, 30);

        txtcat2.setEditable(false);
        txtcat2.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jPanel23.add(txtcat2);
        txtcat2.setBounds(650, 240, 240, 30);

        jLabel67.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel67.setText("Unit of Measure");
        jPanel23.add(jLabel67);
        jLabel67.setBounds(540, 280, 100, 30);

        txtunit2.setEditable(false);
        txtunit2.setText("0");
        jPanel23.add(txtunit2);
        txtunit2.setBounds(650, 280, 240, 30);

        jLabel68.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel68.setText("Quantity");
        jPanel23.add(jLabel68);
        jLabel68.setBounds(540, 320, 90, 30);

        txtqty2.setEditable(false);
        txtqty2.setText("0");
        jPanel23.add(txtqty2);
        txtqty2.setBounds(650, 320, 240, 30);

        jLabel69.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel69.setText("Customer");
        jPanel23.add(jLabel69);
        jLabel69.setBounds(540, 80, 70, 30);

        txtName2.setEditable(false);
        txtName2.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        txtName2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtName2ActionPerformed(evt);
            }
        });
        jPanel23.add(txtName2);
        txtName2.setBounds(650, 80, 240, 30);

        jLabel70.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel70.setText("Contact");
        jPanel23.add(jLabel70);
        jLabel70.setBounds(540, 120, 70, 30);

        txtContact1.setEditable(false);
        txtContact1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel23.add(txtContact1);
        txtContact1.setBounds(650, 120, 240, 30);

        jLabel71.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel71.setText("Address");
        jPanel23.add(jLabel71);
        jLabel71.setBounds(540, 160, 80, 30);

        txtAddress1.setEditable(false);
        txtAddress1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel23.add(txtAddress1);
        txtAddress1.setBounds(650, 160, 240, 30);

        jLabel72.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel72.setText("Delivery Date");
        jPanel23.add(jLabel72);
        jLabel72.setBounds(540, 360, 100, 30);

        btnRemove6.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnRemove6.setText("Clear Order");
        btnRemove6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemove6ActionPerformed(evt);
            }
        });
        jPanel23.add(btnRemove6);
        btnRemove6.setBounds(660, 420, 130, 50);

        txtPDate5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel23.add(txtPDate5);
        txtPDate5.setBounds(650, 360, 240, 30);

        jTextField9.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField9CaretUpdate(evt);
            }
        });
        jPanel23.add(jTextField9);
        jTextField9.setBounds(30, 30, 200, 30);

        jLabel82.setForeground(java.awt.Color.blue);
        jLabel82.setText("* Search Customer");
        jPanel23.add(jLabel82);
        jLabel82.setBounds(240, 30, 130, 14);

        jTabbedPane3.addTab("View All Orders", jPanel23);

        jPanel22.setLayout(null);

        jTable13.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order ID", "Date", "Customer", "Item", "Unit", "Quantity", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable13.getTableHeader().setReorderingAllowed(false);
        jTable13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable13MouseClicked(evt);
            }
        });
        jScrollPane14.setViewportView(jTable13);
        if (jTable13.getColumnModel().getColumnCount() > 0) {
            jTable13.getColumnModel().getColumn(0).setResizable(false);
            jTable13.getColumnModel().getColumn(1).setResizable(false);
            jTable13.getColumnModel().getColumn(2).setResizable(false);
            jTable13.getColumnModel().getColumn(3).setResizable(false);
            jTable13.getColumnModel().getColumn(4).setResizable(false);
            jTable13.getColumnModel().getColumn(5).setResizable(false);
            jTable13.getColumnModel().getColumn(6).setResizable(false);
        }

        jPanel22.add(jScrollPane14);
        jScrollPane14.setBounds(23, 80, 500, 400);

        jLabel73.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel73.setText("Delivery Date");
        jPanel22.add(jLabel73);
        jLabel73.setBounds(540, 360, 100, 30);

        jLabel74.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel74.setText("Quantity");
        jPanel22.add(jLabel74);
        jLabel74.setBounds(540, 320, 90, 30);

        txtqty3.setEditable(false);
        txtqty3.setText("0");
        jPanel22.add(txtqty3);
        txtqty3.setBounds(650, 320, 240, 30);

        txtunit3.setEditable(false);
        txtunit3.setText("0");
        jPanel22.add(txtunit3);
        txtunit3.setBounds(650, 280, 240, 30);

        jLabel75.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel75.setText("Unit of Measure");
        jPanel22.add(jLabel75);
        jLabel75.setBounds(540, 280, 100, 30);

        jLabel76.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel76.setText("Category");
        jPanel22.add(jLabel76);
        jLabel76.setBounds(540, 240, 90, 30);

        txtcat3.setEditable(false);
        txtcat3.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jPanel22.add(txtcat3);
        txtcat3.setBounds(650, 240, 240, 30);

        txtname3.setEditable(false);
        txtname3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtname3CaretUpdate(evt);
            }
        });
        txtname3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtname3ActionPerformed(evt);
            }
        });
        txtname3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtname3KeyReleased(evt);
            }
        });
        jPanel22.add(txtname3);
        txtname3.setBounds(650, 200, 240, 30);

        jLabel77.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel77.setText("Name");
        jPanel22.add(jLabel77);
        jLabel77.setBounds(540, 200, 90, 30);

        jLabel78.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel78.setText("Address");
        jPanel22.add(jLabel78);
        jLabel78.setBounds(540, 160, 80, 30);

        txtAddress2.setEditable(false);
        txtAddress2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel22.add(txtAddress2);
        txtAddress2.setBounds(650, 160, 240, 30);

        txtContact2.setEditable(false);
        txtContact2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel22.add(txtContact2);
        txtContact2.setBounds(650, 120, 240, 30);

        jLabel79.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel79.setText("Contact");
        jPanel22.add(jLabel79);
        jLabel79.setBounds(540, 120, 70, 30);

        jLabel80.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel80.setText("Customer");
        jPanel22.add(jLabel80);
        jLabel80.setBounds(540, 80, 70, 30);

        txtName3.setEditable(false);
        txtName3.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        txtName3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtName3ActionPerformed(evt);
            }
        });
        jPanel22.add(txtName3);
        txtName3.setBounds(650, 80, 240, 30);

        txtPDate3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel22.add(txtPDate3);
        txtPDate3.setBounds(650, 360, 240, 30);

        btnRemove5.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnRemove5.setText("Clear Order");
        btnRemove5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemove5ActionPerformed(evt);
            }
        });
        jPanel22.add(btnRemove5);
        btnRemove5.setBounds(660, 420, 130, 50);

        jLabel83.setForeground(java.awt.Color.blue);
        jLabel83.setText("* Search Customer");
        jPanel22.add(jLabel83);
        jLabel83.setBounds(240, 30, 130, 14);

        jTextField10.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField10CaretUpdate(evt);
            }
        });
        jPanel22.add(jTextField10);
        jTextField10.setBounds(30, 30, 200, 30);

        jLabel86.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel86.setText("Order ID");
        jPanel22.add(jLabel86);
        jLabel86.setBounds(540, 40, 90, 30);

        txtID2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel22.add(txtID2);
        txtID2.setBounds(650, 40, 240, 30);

        jTabbedPane3.addTab("View Due Orders", jPanel22);

        javax.swing.GroupLayout OrderLayout = new javax.swing.GroupLayout(Order.getContentPane());
        Order.getContentPane().setLayout(OrderLayout);
        OrderLayout.setHorizontalGroup(
            OrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderLayout.createSequentialGroup()
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 933, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        OrderLayout.setVerticalGroup(
            OrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
        );

        NewSale.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        NewSale.setMinimumSize(new java.awt.Dimension(960, 606));
        NewSale.setModal(true);
        NewSale.setResizable(false);

        jPanel16.setLayout(null);

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Item Manager", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Lucida Calligraphy", 0, 12))); // NOI18N
        jPanel17.setLayout(null);

        jLabel27.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel27.setText("Date");
        jPanel17.add(jLabel27);
        jLabel27.setBounds(30, 20, 60, 30);

        jLabel28.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel28.setText("Item");
        jPanel17.add(jLabel28);
        jLabel28.setBounds(30, 60, 50, 30);

        jLabel29.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel29.setText("Category");
        jPanel17.add(jLabel29);
        jLabel29.setBounds(30, 100, 90, 30);

        jLabel33.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel33.setText("Unit of Measure");
        jPanel17.add(jLabel33);
        jLabel33.setBounds(30, 140, 100, 30);

        jLabel34.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel34.setText("Quantity");
        jPanel17.add(jLabel34);
        jLabel34.setBounds(30, 180, 90, 30);

        jLabel36.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel36.setText("Unit Price");
        jPanel17.add(jLabel36);
        jLabel36.setBounds(30, 220, 70, 30);

        txtname1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtname1CaretUpdate(evt);
            }
        });
        txtname1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtname1ActionPerformed(evt);
            }
        });
        txtname1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtname1KeyReleased(evt);
            }
        });
        jPanel17.add(txtname1);
        txtname1.setBounds(140, 60, 210, 30);

        txtcat1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jPanel17.add(txtcat1);
        txtcat1.setBounds(140, 100, 240, 30);
        jPanel17.add(txtunit1);
        txtunit1.setBounds(140, 140, 240, 30);

        txttotal1.setText("0");
        txttotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal1ActionPerformed(evt);
            }
        });
        jPanel17.add(txttotal1);
        txttotal1.setBounds(140, 220, 240, 30);

        txtqty1.setText("0");
        jPanel17.add(txtqty1);
        txtqty1.setBounds(140, 180, 240, 30);

        btnSearch3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/search.png"))); // NOI18N
        btnSearch3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch3ActionPerformed(evt);
            }
        });
        jPanel17.add(btnSearch3);
        btnSearch3.setBounds(350, 60, 32, 30);

        txtPDate2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel17.add(txtPDate2);
        txtPDate2.setBounds(90, 20, 40, 30);
        jPanel17.add(jDateChooser6);
        jDateChooser6.setBounds(140, 20, 240, 30);

        jPanel16.add(jPanel17);
        jPanel17.setBounds(40, 70, 410, 260);

        jTable9.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "Name", "Category", "Unit", "Quantity", "Total", "Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable9.getTableHeader().setReorderingAllowed(false);
        jTable9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable9MouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(jTable9);
        if (jTable9.getColumnModel().getColumnCount() > 0) {
            jTable9.getColumnModel().getColumn(0).setResizable(false);
            jTable9.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable9.getColumnModel().getColumn(1).setResizable(false);
            jTable9.getColumnModel().getColumn(2).setResizable(false);
            jTable9.getColumnModel().getColumn(3).setResizable(false);
            jTable9.getColumnModel().getColumn(4).setResizable(false);
            jTable9.getColumnModel().getColumn(5).setResizable(false);
            jTable9.getColumnModel().getColumn(6).setResizable(false);
            jTable9.getColumnModel().getColumn(6).setPreferredWidth(0);
        }

        jPanel16.add(jScrollPane7);
        jScrollPane7.setBounds(480, 80, 410, 360);

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Payment Manager", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Lucida Calligraphy", 0, 12))); // NOI18N
        jPanel18.setLayout(null);

        jComboBox2.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "Lipa na M-Pesa", "Other Means" }));
        jPanel18.add(jComboBox2);
        jComboBox2.setBounds(150, 20, 190, 30);

        jLabel57.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel57.setText("Amount Due");
        jPanel18.add(jLabel57);
        jLabel57.setBounds(130, 70, 100, 30);

        txtPAmnt2.setFont(new java.awt.Font("Cantarell", 0, 36)); // NOI18N
        txtPAmnt2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt2.setText("0");
        txtPAmnt2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel18.add(txtPAmnt2);
        txtPAmnt2.setBounds(60, 100, 260, 50);

        jLabel50.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel50.setText("Mode Of Payment");
        jPanel18.add(jLabel50);
        jLabel50.setBounds(20, 20, 130, 30);

        jPanel16.add(jPanel18);
        jPanel18.setBounds(40, 350, 410, 170);

        btnAdd1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnAdd1.setText("Add");
        btnAdd1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdd1ActionPerformed(evt);
            }
        });
        jPanel16.add(btnAdd1);
        btnAdd1.setBounds(500, 460, 78, 50);

        btnUpdateItem1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnUpdateItem1.setText("Update");
        btnUpdateItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateItem1ActionPerformed(evt);
            }
        });
        jPanel16.add(btnUpdateItem1);
        btnUpdateItem1.setBounds(600, 460, 78, 50);

        btnRemove1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnRemove1.setText("Delete");
        btnRemove1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemove1ActionPerformed(evt);
            }
        });
        jPanel16.add(btnRemove1);
        btnRemove1.setBounds(700, 460, 78, 50);

        btnProcess1.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        btnProcess1.setText("Process");
        btnProcess1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcess1ActionPerformed(evt);
            }
        });
        jPanel16.add(btnProcess1);
        btnProcess1.setBounds(800, 460, 78, 50);

        lblUser4.setFont(new java.awt.Font("Lucida Calligraphy", 0, 30)); // NOI18N
        lblUser4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUser4.setText("Item Sales ");
        jPanel16.add(lblUser4);
        lblUser4.setBounds(350, 0, 280, 50);

        javax.swing.GroupLayout NewSaleLayout = new javax.swing.GroupLayout(NewSale.getContentPane());
        NewSale.getContentPane().setLayout(NewSaleLayout);
        NewSaleLayout.setHorizontalGroup(
            NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 960, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        NewSaleLayout.setVerticalGroup(
            NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NewSaleLayout.createSequentialGroup()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 606, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        OrderCustomer.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        OrderCustomer.setMinimumSize(new java.awt.Dimension(424, 409));
        OrderCustomer.setModal(true);
        OrderCustomer.setResizable(false);

        jPanel12.setLayout(null);

        jTable7.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer", "Contact", "Address"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable7.getTableHeader().setReorderingAllowed(false);
        jTable7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable7MouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTable7);
        if (jTable7.getColumnModel().getColumnCount() > 0) {
            jTable7.getColumnModel().getColumn(0).setResizable(false);
            jTable7.getColumnModel().getColumn(1).setResizable(false);
            jTable7.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel12.add(jScrollPane5);
        jScrollPane5.setBounds(10, 100, 400, 270);

        jTextField7.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField7CaretUpdate(evt);
            }
        });
        jPanel12.add(jTextField7);
        jTextField7.setBounds(20, 60, 190, 30);

        jLabel3.setForeground(java.awt.Color.blue);
        jLabel3.setText("* Search Customer");
        jPanel12.add(jLabel3);
        jLabel3.setBounds(220, 60, 160, 20);

        jLabel10.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Customer Table");
        jPanel12.add(jLabel10);
        jLabel10.setBounds(80, 10, 230, 30);

        javax.swing.GroupLayout OrderCustomerLayout = new javax.swing.GroupLayout(OrderCustomer.getContentPane());
        OrderCustomer.getContentPane().setLayout(OrderCustomerLayout);
        OrderCustomerLayout.setHorizontalGroup(
            OrderCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        OrderCustomerLayout.setVerticalGroup(
            OrderCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderCustomerLayout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        SaleItems.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        SaleItems.setMinimumSize(new java.awt.Dimension(424, 410));
        SaleItems.setModal(true);
        SaleItems.setResizable(false);

        jPanel19.setLayout(null);

        jTable10.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Category", "Units"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable10.getTableHeader().setReorderingAllowed(false);
        jTable10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable10MouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(jTable10);
        if (jTable10.getColumnModel().getColumnCount() > 0) {
            jTable10.getColumnModel().getColumn(0).setResizable(false);
            jTable10.getColumnModel().getColumn(1).setResizable(false);
            jTable10.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel19.add(jScrollPane11);
        jScrollPane11.setBounds(10, 100, 400, 270);

        jTextField8.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField8CaretUpdate(evt);
            }
        });
        jPanel19.add(jTextField8);
        jTextField8.setBounds(20, 60, 190, 30);

        jLabel51.setForeground(java.awt.Color.blue);
        jLabel51.setText("* Search Item");
        jPanel19.add(jLabel51);
        jLabel51.setBounds(220, 60, 160, 20);

        jLabel52.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("Item Table");
        jPanel19.add(jLabel52);
        jLabel52.setBounds(80, 10, 230, 30);

        javax.swing.GroupLayout SaleItemsLayout = new javax.swing.GroupLayout(SaleItems.getContentPane());
        SaleItems.getContentPane().setLayout(SaleItemsLayout);
        SaleItemsLayout.setHorizontalGroup(
            SaleItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        SaleItemsLayout.setVerticalGroup(
            SaleItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SaleItemsLayout.createSequentialGroup()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        OrderItems.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        OrderItems.setMinimumSize(new java.awt.Dimension(424, 410));
        OrderItems.setModal(true);
        OrderItems.setResizable(false);

        jPanel24.setLayout(null);

        jTable12.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Category", "Units"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable12.getTableHeader().setReorderingAllowed(false);
        jTable12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable12MouseClicked(evt);
            }
        });
        jScrollPane13.setViewportView(jTable12);
        if (jTable12.getColumnModel().getColumnCount() > 0) {
            jTable12.getColumnModel().getColumn(0).setResizable(false);
            jTable12.getColumnModel().getColumn(1).setResizable(false);
            jTable12.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel24.add(jScrollPane13);
        jScrollPane13.setBounds(10, 100, 400, 270);

        jTextField11.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField11CaretUpdate(evt);
            }
        });
        jPanel24.add(jTextField11);
        jTextField11.setBounds(20, 60, 190, 30);

        jLabel84.setForeground(java.awt.Color.blue);
        jLabel84.setText("* Search Item");
        jPanel24.add(jLabel84);
        jLabel84.setBounds(220, 60, 160, 20);

        jLabel85.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel85.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel85.setText("Item Table");
        jPanel24.add(jLabel85);
        jLabel85.setBounds(80, 10, 230, 30);

        javax.swing.GroupLayout OrderItemsLayout = new javax.swing.GroupLayout(OrderItems.getContentPane());
        OrderItems.getContentPane().setLayout(OrderItemsLayout);
        OrderItemsLayout.setHorizontalGroup(
            OrderItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        OrderItemsLayout.setVerticalGroup(
            OrderItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderItemsLayout.createSequentialGroup()
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        SalePayment.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        SalePayment.setMinimumSize(new java.awt.Dimension(327, 378));
        SalePayment.setModal(true);
        SalePayment.setResizable(false);

        jPanel20.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel20.setLayout(null);

        jLabel53.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel53.setText("Amount Due");
        jPanel20.add(jLabel53);
        jLabel53.setBounds(110, 50, 100, 30);

        txtPAmnt.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtPAmnt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt.setText("0");
        txtPAmnt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(txtPAmnt);
        txtPAmnt.setBounds(70, 80, 200, 40);

        jLabel54.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("Sale Payment");
        jLabel54.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(jLabel54);
        jLabel54.setBounds(0, 0, 330, 40);

        txtTendered.setBackground(new java.awt.Color(46, 66, 99));
        txtTendered.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTendered.setForeground(new java.awt.Color(255, 255, 255));
        txtTendered.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTendered.setText("0");
        txtTendered.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtTenderedCaretUpdate(evt);
            }
        });
        jPanel20.add(txtTendered);
        txtTendered.setBounds(130, 220, 180, 30);

        jLabel55.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("Balance");
        jPanel20.add(jLabel55);
        jLabel55.setBounds(110, 130, 100, 30);

        txtBal.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtBal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtBal.setText("0");
        txtBal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(txtBal);
        txtBal.setBounds(70, 160, 200, 40);

        jLabel64.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel64.setText("Amount Tendered");
        jPanel20.add(jLabel64);
        jLabel64.setBounds(10, 220, 140, 30);

        btnPrint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPrint.setText("Submit");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        jPanel20.add(btnPrint);
        btnPrint.setBounds(90, 280, 130, 50);
        jPanel20.add(jLabel1);
        jLabel1.setBounds(10, 140, 40, 30);

        javax.swing.GroupLayout SalePaymentLayout = new javax.swing.GroupLayout(SalePayment.getContentPane());
        SalePayment.getContentPane().setLayout(SalePaymentLayout);
        SalePaymentLayout.setHorizontalGroup(
            SalePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        SalePaymentLayout.setVerticalGroup(
            SalePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SalePaymentLayout.createSequentialGroup()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        OrderPayment.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        OrderPayment.setMinimumSize(new java.awt.Dimension(327, 365));
        OrderPayment.setModal(true);
        OrderPayment.setResizable(false);

        jPanel21.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel21.setLayout(null);

        jLabel58.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("Amount Due");
        jPanel21.add(jLabel58);
        jLabel58.setBounds(110, 50, 100, 30);

        txtPAmnt3.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtPAmnt3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt3.setText("0");
        txtPAmnt3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel21.add(txtPAmnt3);
        txtPAmnt3.setBounds(70, 80, 200, 40);

        jLabel59.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel59.setText("Order Payment");
        jLabel59.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel21.add(jLabel59);
        jLabel59.setBounds(0, 0, 330, 40);

        txtTendered1.setBackground(new java.awt.Color(46, 66, 99));
        txtTendered1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTendered1.setForeground(new java.awt.Color(255, 255, 255));
        txtTendered1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTendered1.setText("0");
        txtTendered1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtTendered1CaretUpdate(evt);
            }
        });
        jPanel21.add(txtTendered1);
        txtTendered1.setBounds(130, 220, 170, 30);

        jLabel60.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel60.setText("Balance");
        jPanel21.add(jLabel60);
        jLabel60.setBounds(110, 130, 100, 30);

        txtBal1.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtBal1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtBal1.setText("0");
        txtBal1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel21.add(txtBal1);
        txtBal1.setBounds(70, 160, 200, 40);

        btnSubmit1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSubmit1.setText("Submit");
        btnSubmit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmit1ActionPerformed(evt);
            }
        });
        jPanel21.add(btnSubmit1);
        btnSubmit1.setBounds(100, 270, 110, 50);

        jLabel65.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel65.setText("Amount Tendered");
        jPanel21.add(jLabel65);
        jLabel65.setBounds(10, 220, 140, 30);
        jPanel21.add(jLabel61);
        jLabel61.setBounds(10, 140, 40, 30);

        javax.swing.GroupLayout OrderPaymentLayout = new javax.swing.GroupLayout(OrderPayment.getContentPane());
        OrderPayment.getContentPane().setLayout(OrderPaymentLayout);
        OrderPaymentLayout.setHorizontalGroup(
            OrderPaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        OrderPaymentLayout.setVerticalGroup(
            OrderPaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPaymentLayout.createSequentialGroup()
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        MakePayment.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        MakePayment.setMinimumSize(new java.awt.Dimension(327, 365));
        MakePayment.setModal(true);
        MakePayment.setResizable(false);

        jPanel25.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel25.setLayout(null);

        jLabel87.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel87.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel87.setText("Amount Due");
        jPanel25.add(jLabel87);
        jLabel87.setBounds(110, 50, 100, 30);

        txtPAmnt4.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtPAmnt4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt4.setText("0");
        txtPAmnt4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel25.add(txtPAmnt4);
        txtPAmnt4.setBounds(70, 80, 200, 40);

        jLabel88.setFont(new java.awt.Font("Lucida Calligraphy", 0, 24)); // NOI18N
        jLabel88.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel88.setText("Make Payment");
        jLabel88.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel25.add(jLabel88);
        jLabel88.setBounds(0, 0, 330, 40);

        txtTendered2.setBackground(new java.awt.Color(46, 66, 99));
        txtTendered2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTendered2.setForeground(new java.awt.Color(255, 255, 255));
        txtTendered2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTendered2.setText("0");
        txtTendered2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtTendered2CaretUpdate(evt);
            }
        });
        jPanel25.add(txtTendered2);
        txtTendered2.setBounds(130, 220, 170, 30);

        jLabel89.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel89.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel89.setText("Balance");
        jPanel25.add(jLabel89);
        jLabel89.setBounds(110, 130, 100, 30);

        txtBal2.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtBal2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtBal2.setText("0");
        txtBal2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel25.add(txtBal2);
        txtBal2.setBounds(70, 160, 200, 40);

        btnSubmit2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSubmit2.setText("Submit");
        btnSubmit2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmit2ActionPerformed(evt);
            }
        });
        jPanel25.add(btnSubmit2);
        btnSubmit2.setBounds(100, 270, 110, 50);

        jLabel90.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel90.setText("Amount Tendered");
        jPanel25.add(jLabel90);
        jLabel90.setBounds(10, 220, 140, 30);
        jPanel25.add(jLabel91);
        jLabel91.setBounds(10, 140, 40, 30);

        javax.swing.GroupLayout MakePaymentLayout = new javax.swing.GroupLayout(MakePayment.getContentPane());
        MakePayment.getContentPane().setLayout(MakePaymentLayout);
        MakePaymentLayout.setHorizontalGroup(
            MakePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        MakePaymentLayout.setVerticalGroup(
            MakePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MakePaymentLayout.createSequentialGroup()
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);

        jPanel1.setLayout(null);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(0, 0, 1370, 20);

        lblUser1.setFont(new java.awt.Font("Lucida Calligraphy", 1, 18)); // NOI18N
        lblUser1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUser1.setText("Administrator");
        jPanel1.add(lblUser1);
        lblUser1.setBounds(1200, 30, 170, 30);

        lblDate.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        lblDate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDate.setText("Date");
        jPanel1.add(lblDate);
        lblDate.setBounds(940, 10, 170, 20);

        lblTime.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
        lblTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTime.setText("Time");
        jPanel1.add(lblTime);
        lblTime.setBounds(730, 10, 130, 20);

        jLabel2.setFont(new java.awt.Font("Castellar", 1, 36)); // NOI18N
        jLabel2.setText("SWEET LULU INVESTMENTS");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(0, 10, 600, 50);

        lblUser2.setFont(new java.awt.Font("Corbel", 1, 18)); // NOI18N
        lblUser2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUser2.setText("Logged in as");
        jPanel1.add(lblUser2);
        lblUser2.setBounds(1200, 0, 170, 30);

        btnNewSale.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/foto-klubnichniy-mafin-dieta-kak-pohudet.png"))); // NOI18N
        btnNewSale.setOpaque(false);
        btnNewSale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewSaleActionPerformed(evt);
            }
        });
        jPanel1.add(btnNewSale);
        btnNewSale.setBounds(20, 80, 142, 102);

        btnExpense.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/kid2.png"))); // NOI18N
        btnExpense.setOpaque(false);
        btnExpense.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpenseActionPerformed(evt);
            }
        });
        jPanel1.add(btnExpense);
        btnExpense.setBounds(530, 80, 142, 102);

        btnOrder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/pastry-chef-holding-a-cake-300x300.jpg"))); // NOI18N
        btnOrder.setOpaque(false);
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });
        jPanel1.add(btnOrder);
        btnOrder.setBounds(190, 80, 142, 102);

        btnPassword.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/forgetpassword.png"))); // NOI18N
        btnPassword.setOpaque(false);
        btnPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPasswordActionPerformed(evt);
            }
        });
        jPanel1.add(btnPassword);
        btnPassword.setBounds(870, 80, 142, 102);

        btnRegister.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/favicon.ico.jpg"))); // NOI18N
        btnRegister.setOpaque(false);
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegister);
        btnRegister.setBounds(1040, 80, 142, 102);

        btnMenus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/imagesO0H19C40.png"))); // NOI18N
        btnMenus.setOpaque(false);
        btnMenus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenusActionPerformed(evt);
            }
        });
        jPanel1.add(btnMenus);
        btnMenus.setBounds(360, 80, 142, 102);

        btnReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/untitled.png"))); // NOI18N
        btnReport.setOpaque(false);
        btnReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportActionPerformed(evt);
            }
        });
        jPanel1.add(btnReport);
        btnReport.setBounds(700, 80, 142, 102);

        jLabel4.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(880, 10, 60, 20);

        jLabel5.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Time:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(670, 10, 60, 20);

        btnLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logout-icon.png"))); // NOI18N
        btnLogout.setToolTipText("");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogout);
        btnLogout.setBounds(1210, 80, 142, 102);
        jPanel1.add(jDateChooser9);
        jDateChooser9.setBounds(40, 220, 230, 30);

        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jMenu1.setText("File");
        jMenu1.setToolTipText("");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logout-xxl.png"))); // NOI18N
        jMenuItem3.setText("Log Out");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, java.awt.event.InputEvent.SHIFT_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/close-box-red.png"))); // NOI18N
        jMenuItem2.setText("Exit");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Tools");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/calculator_icon.jpg"))); // NOI18N
        jMenuItem1.setText("Calculator");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem1);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1370, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 709, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
         System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btnMenusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenusActionPerformed
        // TODO add your handling code here:
        Menus.setLocationRelativeTo(null);
        Menus.setVisible(true);
    }//GEN-LAST:event_btnMenusActionPerformed

    private void btnReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportActionPerformed
        // TODO add your handling code here:
        Report.setLocationRelativeTo(null);
        Report.setVisible(true);
    }//GEN-LAST:event_btnReportActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        LogIn L = new LogIn();
            L.setVisible(true);
            this.setVisible(false);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void btnUpdatePassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePassActionPerformed
        // TODO add your handling code here:
        // Confirming the existing password
        Connection con = getConnection();
        try {
            if (log.AccessType == "Administrator") {
                ps = con.prepareStatement("SELECT `Admin_Name`, `Admin_Password` FROM `administrator` WHERE BINARY `Admin_Name`=? AND BINARY `Admin_Password`=?");
                ps.setString(1, log.name);
                ps.setString(2, String.valueOf(txtcurr.getPassword()));
                ResultSet result = ps.executeQuery();
                if (txtcurr.getText().length() > 0) {
                    if (result.next()) {
                        ChangePass();
                        Change_Pass.dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Incorrect Password");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Empty Fields Detected!");
                }
                
            } else {
                if (log.AccessType == "Employee") {
                    ps = con.prepareStatement( "SELECT `First_Name`, `Password` FROM `user` WHERE BINARY `First_Name`=? AND BINARY `Password`=?");
                    ps.setString(1, log.name);
                    ps.setString(2, String.valueOf(txtcurr.getPassword()));
                    ResultSet result = ps.executeQuery();
                    if (txtcurr.getText().length() > 0) {
                        if (result.next()) {
                            ChangePass();
                            Change_Pass.dispose();
                        } else {
                            JOptionPane.showMessageDialog(null, "Incorrect Password");

                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "EMPTY FIELDS DETECTED! PLEASE FILL IN THE DETAILS");
                    }
                }
            }
        } catch (SQLException ex) {
            // JOptionPane.showMessageDialog(this, ex.getMessage());
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnUpdatePassActionPerformed

    private void jCheckBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox4ActionPerformed
        if (jCheckBox4.isSelected())
        {
            txtcurr.setEchoChar((char)0);
            txtnew.setEchoChar((char)0);
            txtconfirm.setEchoChar((char)0);
        }
        else{
            txtcurr.setEchoChar('*');
            txtnew.setEchoChar('*');
            txtconfirm.setEchoChar('*');
        }
    }//GEN-LAST:event_jCheckBox4ActionPerformed

    private void btnPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPasswordActionPerformed
        // TODO add your handling code here:
        Change_Pass.setLocationRelativeTo(null);
        Change_Pass.setVisible(true);
    }//GEN-LAST:event_btnPasswordActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        // TODO add your handling code here:
        Accounts.setLocationRelativeTo(null);
        Accounts.setVisible(true);
    }//GEN-LAST:event_btnRegisterActionPerformed

    private void jTable5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable5MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)jTable5.getModel();
        int selectedRowIndex = jTable5.getSelectedRow();

        // set the selected row data into jtextfields
        txtExpense.setText(model.getValueAt(selectedRowIndex, 1).toString());
        txtAmnt.setText(model.getValueAt(selectedRowIndex, 2).toString());
        if (model.getValueAt(selectedRowIndex, 3) == null) {
            txtInfo.setText(null);
        } else {
            txtInfo.setText(model.getValueAt(selectedRowIndex, 3).toString());
        }
    }//GEN-LAST:event_jTable5MouseClicked

    private void btnSave2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave2ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser8.getDate());
        if (txtExpense.getText().equals("") || txtAmnt.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Fill in the Expense and Amount!");
        } else {
            try {
                ps = con.prepareStatement("INSERT INTO expenses (Date, Expense, Amount, Other_Information)" + " values(?, ?, ?, ?)");
                ps.setString(1, addDate);
                ps.setString(2, txtExpense.getText());
                ps.setInt(3, Integer.parseInt(txtAmnt.getText()));
                ps.setString(4, txtInfo.getText());

                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "Record saved");
                FilterTable5(jTable5, sql, jDateChooser9.getDate(), jDateChooser9.getDate());
                lblTotal1.setText(Integer.toString(sum.getSum(jTable5, 2)));

                txtInfo.setText(null);
                txtAmnt.setText("0");
                txtExpense.setText(null);
                Date();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnSave2ActionPerformed

    private void btnDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelete1ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        DefaultTableModel model = (DefaultTableModel)jTable5.getModel();
        int selectedRowIndex = jTable5.getSelectedRow();
        String dates = model.getValueAt(selectedRowIndex, 0).toString();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser8.getDate());
        try {
            ps = con.prepareStatement("DELETE FROM expenses WHERE Expense = ? AND Date = ?");
            ps.setString(1, txtExpense.getText());
            ps.setString(2, dates);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record Deleted");
             FilterTable5(jTable5, sql, jDateChooser9.getDate(), jDateChooser9.getDate());
             lblTotal1.setText(Integer.toString(sum.getSum(jTable5, 2)));
            txtInfo.setText(null);
            txtAmnt.setText("0");
            txtExpense.setText(null);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnDelete1ActionPerformed

    private void btnCancel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel1ActionPerformed
        // TODO add your handling code here:
        Date date = new Date();
        Date();
        txtInfo.setText(null);
        txtAmnt.setText("0");
        txtExpense.setText(null);
    }//GEN-LAST:event_btnCancel1ActionPerformed

    private void txtAmntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmntActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAmntActionPerformed

    private void jTextField3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField3CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable5, select, jTextField3);
        lblTotal1.setText(Integer.toString(sum.getSum(jTable5, 2)));
    }//GEN-LAST:event_jTextField3CaretUpdate

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jButton30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton30ActionPerformed
        // TODO add your handling code here:
        if (jDateChooser2.getDate() == null || jDateChooser1.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please set the required dates");
        } else {
            
            FilterTable5(jTable5, sql, jDateChooser2.getDate(), jDateChooser1.getDate());
            lblTotal1.setText(Integer.toString(sum.getSum(jTable5, 2)));
        }
    }//GEN-LAST:event_jButton30ActionPerformed

    private void btnExpenseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpenseActionPerformed
        // TODO add your handling code here:
        Date();
        Expenses.setLocationRelativeTo(null);
        Expenses.setVisible(true);
    }//GEN-LAST:event_btnExpenseActionPerformed

    private void jTextField4CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField4CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable6, report, jTextField4);
        lblTotal.setText(Integer.toString(sum.getSum(jTable6, 6)));
    }//GEN-LAST:event_jTextField4CaretUpdate

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton31ActionPerformed
        // TODO add your handling code here:
        if (jDateChooser4.getDate() == null || jDateChooser3.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please set the required dates");
        } else {
            
            FilterTable5(jTable6, sql1, jDateChooser4.getDate(), jDateChooser3.getDate());
            lblTotal.setText(Integer.toString(sum.getSum(jTable6, 6)));
        }
    }//GEN-LAST:event_jButton31ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        arithmetic_operation();
        jLabel35.setText(null);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton26ActionPerformed
        // TODO add your handling code here:
        num = Double.parseDouble(jTextField2.getText());
        calculation = 1;
        jTextField2.setText(null);
        jLabel35.setText(num + " +");
    }//GEN-LAST:event_jButton26ActionPerformed

    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + ".");
    }//GEN-LAST:event_jButton27ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "0");
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "00");
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "1");
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "2");
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "3");
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        // TODO add your handling code here:
        num = Double.parseDouble(jTextField2.getText());
        calculation = 2;
        jTextField2.setText(null);
        jLabel35.setText(num + " -");
    }//GEN-LAST:event_jButton25ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        // TODO add your handling code here:
        num = Double.parseDouble(jTextField2.getText());
        calculation = 3;
        jTextField2.setText(null);
        jLabel35.setText(num + " *");
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "6");
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "5");
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "4");
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "7");
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "8");
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(jTextField2.getText() + "9");
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
        // TODO add your handling code here:
        num = Double.parseDouble(jTextField2.getText());
        calculation = 4;
        jTextField2.setText(null);
        jLabel35.setText(num + " /");
    }//GEN-LAST:event_jButton28ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        jTextField2.setText(null);
        jLabel35.setText(null);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        double c = Double.parseDouble(jTextField2.getText());
        if (c < 0) {
            c = -c;
            jTextField2.setText(Double.toString(c));
        } else {
            jTextField2.setText("-" + jTextField2.getText());
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int lengh = jTextField2.getText().length();
        int num = jTextField2.getText().length() - 1;
        String store;
        if (lengh > 0) {
            StringBuilder back = new StringBuilder (jTextField2.getText());
            back.deleteCharAt(num);
            store = back.toString();
            jTextField2.setText(store);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
//        Calculator.setLocationRelativeTo(null);
        Calculator.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        txtID1.setText(null);
        txtCat1.setText(null);
        btnSave4.setEnabled(true);
        btnDel2.setEnabled(false);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        txtName.setText(null);
        txtUnit.setText(null);
        txtPrice.setText(null);
        btnSave3.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDel1.setEnabled(false);
    }//GEN-LAST:event_btnNewActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        btnDel2.setEnabled(true);
        btnSave4.setEnabled(false);
          model = (DefaultTableModel) jTable1.getModel();
        int selectedRowIndex = jTable1.getSelectedRow();
         txtID1.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtCat1.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTextField6CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField6CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable1, menu, jTextField6);
    }//GEN-LAST:event_jTextField6CaretUpdate

    private void jTextField5CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField5CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable2, item, jTextField5);
    }//GEN-LAST:event_jTextField5CaretUpdate

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        btnUpdate.setEnabled(true);
        btnDel1.setEnabled(true);
        btnSave3.setEnabled(false);
         model = (DefaultTableModel) jTable2.getModel();
        int selectedRowIndex = jTable2.getSelectedRow();
         txtName.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtCat.setSelectedItem(model.getValueAt(selectedRowIndex, 1).toString());
         txtUnit.setText(model.getValueAt(selectedRowIndex, 2).toString());
          txtPrice.setText(model.getValueAt(selectedRowIndex, 3).toString());
    }//GEN-LAST:event_jTable2MouseClicked

    private void btnDel2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel2ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        try {
            ps = con.prepareStatement("DELETE FROM menu_category WHERE Category = ?");
            String id = txtCat1.getText();
            ps.setString(1, id);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record Deleted");
            fillCombo ();
            table.FillTable(jTable1, menu, jTextField6);
            txtID1.setText(null);
        txtCat1.setText(null);
        btnSave4.setEnabled(true);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnDel2ActionPerformed

    private void btnDel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel1ActionPerformed
        // TODO add your handling code here:
         Connection con = getConnection();
        try {
            ps = con.prepareStatement("DELETE FROM item_manager WHERE Name = ? AND Unit_of_measurement = ?");
            String id = txtName.getText();
            ps.setString(1, id);
            ps.setString(2, txtUnit.getText());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record Deleted");
            table.FillTable(jTable2, item, jTextField5);
            btnSave3.setEnabled(true);
            txtName.setText(null);
        txtUnit.setText(null);
        txtPrice.setText(null);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnDel1ActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
         if (txtName.getText().equals("") || txtUnit.getText().equals("") || txtPrice.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some Fields are Empty!");
        } else {
            Connection con = getConnection();
                try {
                    String query = "UPDATE item_manager SET Name = ?, Category = ?, Unit_of_Measurement = ?, Price = ? WHERE Name = ?";
                    ps = con.prepareStatement(query);
                    ps.setString(1, txtName.getText());
                    ps.setObject(2, txtCat.getSelectedItem());
                    ps.setString(3, txtUnit.getText());
                    ps.setInt(4, Integer.parseInt(txtPrice.getText()));
                    ps.setString(5, txtName.getText());
                    ps.executeUpdate();

                    JOptionPane.showMessageDialog(null, "Record Update Successful");
                    table.FillTable(jTable2, item, jTextField5);
                    btnSave3.setEnabled(true);
                    txtName.setText(null);
        txtUnit.setText(null);
        txtPrice.setText(null);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                } finally {
                    if (rs != null) {
                        try {
                            rs.close();
                        } catch (SQLException e) { /* ignored */}
                    }
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) { /* ignored */}
                    }
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) { /* ignored */}
                    }
                }
         }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSave3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave3ActionPerformed
        // TODO add your handling code here:
         Connection con = getConnection();
       if (txtName.getText().equals("") || txtUnit.getText().equals("") || txtPrice.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some Fields are Empty!");
        } else {
            try {
                        ps = con.prepareStatement("INSERT INTO item_manager (Name, Category, Unit_of_measurement, Price)" + " values(?, ?, ?, ?)");
                        ps.setString (1, txtName.getText());
                        ps.setObject(2, txtCat.getSelectedItem());
                        ps.setString (3, txtUnit.getText());
                        ps.setInt (4, Integer.parseInt(txtPrice.getText()));
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record saved");
                        table.FillTable(jTable2, item, jTextField5);
                        txtName.setText(null);
        txtUnit.setText(null);
        txtPrice.setText(null);
        btnUpdate.setEnabled(true);
        btnDel1.setEnabled(true);
                          } catch (HeadlessException | NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnSave3ActionPerformed

    private void btnSave4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave4ActionPerformed
        // TODO add your handling code here:
         Connection con = getConnection();
       if (txtCat1.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the category");
        } else {
            try {
                        ps = con.prepareStatement("INSERT INTO menu_category (Category)" + " values(?)");
                        ps.setString (1, txtCat1.getText());
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record saved");

                        table.FillTable(jTable1, menu, jTextField6);
                        fillCombo ();
                        txtName.setText(null);
        txtID1.setText(null);
        txtCat1.setText(null);
        btnDel2.setEnabled(true);
                          } catch (HeadlessException | NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnSave4ActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        LogIn L = new LogIn();
            L.setVisible(true);
            this.setVisible(false);
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        // TODO add your handling code here:
        Order.setLocationRelativeTo(null);
        Order.setVisible(true);
    }//GEN-LAST:event_btnOrderActionPerformed

    private void btnNewSaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewSaleActionPerformed
        // TODO add your handling code here:
        NewSale.setLocationRelativeTo(null);
        NewSale.setVisible(true);
    }//GEN-LAST:event_btnNewSaleActionPerformed

    private void jTable7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable7MouseClicked
        // TODO add your handling code here:
        int index = jTable7.getSelectedRow();
        TableModel models = jTable7.getModel();
        String customer = models.getValueAt(index, 0).toString();
        String contact = models.getValueAt(index, 1).toString();
        String address = models.getValueAt(index, 2).toString();
        txtName1.setText(customer);
        txtContact.setText(contact);
        txtAddress.setText(address);
        OrderCustomer.dispose();
    }//GEN-LAST:event_jTable7MouseClicked

    private void jTextField7CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField7CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable7, cust, jTextField7);
    }//GEN-LAST:event_jTextField7CaretUpdate

    private void txtnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnameKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnameKeyReleased

    private void txtName1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtName1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtName1ActionPerformed

    private void btnSearch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch1ActionPerformed
        // TODO add your handling code here:
        table.FillTable(jTable7, cust, jTextField7);
         OrderCustomer.setLocationRelativeTo(null);
        OrderCustomer.setVisible(true);
    }//GEN-LAST:event_btnSearch1ActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        txtPAmnt1.setText(Integer.toString(sum.getSum(jTable8, 4)));
        if (txtname.getText().equals("") || txtcat.getText().equals("") || txtunit.getText().equals("") || txtqty.getText().equals("") || txttotal.getText().equals("") || txtName1.getText().equals("") || jDateChooser5.getDate() == null) {
        JOptionPane.showMessageDialog(null, "Some fields are empty or you have not added the customer");
        } else  {
            Connection con = getConnection();
             SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser7.getDate());
                String addDates = date.format(jDateChooser5.getDate());
        array [0] = Integer.parseInt(txtqty.getText());
        array [1] = Integer.parseInt(txttotal.getText());
        array [2] = (array [0] * array [1]);
        
        try {
            String x= txtcat.getText();
        ps = con.prepareStatement("INSERT INTO sales (`Purchase_Date`, Time, `Name`, `Category`, `Unit`, `Quantity`, `Selling_Price`, `Total`, `Mode_Of_Payment`)" + " values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
                     ps.setString(1, addDate);
                    ps.setString (2, lblTime.getText());
                    ps.setString (3, txtname.getText());
                    ps.setString (4, x);
                    ps.setString (5, txtunit.getText());
                    ps.setInt (6, Integer.parseInt(txtqty.getText()));
                    ps.setInt (7, Integer.parseInt(txttotal.getText()));
                    ps.setInt(8, array [2]);
                    ps.setObject(9, jComboBox1.getSelectedItem());
                    ps.executeUpdate();
                    
                     String y = txtcat.getText();
        ps = con.prepareStatement("INSERT INTO orders (`Date`, Time, `Customer`, `Contact`, `Address`, `Name`, `Category`, `Unit_of_measurement`, `Quantity`, `Total`, `Delivery_Date`)" + " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    ps.setString(1, addDate);
                    ps.setString (2, lblTime.getText());
                    ps.setString (3, txtName1.getText());
                    ps.setString (4, txtContact.getText());
                    ps.setString (5, txtAddress.getText());
                    ps.setString (6, txtname.getText());
                    ps.setString (7, y);
                    ps.setString (8, txtunit.getText());
                    ps.setInt (9, Integer.parseInt(txtqty.getText()));
                    ps.setInt(10, array [2]);
                ps.setString(11, addDates);
                    ps.executeUpdate();
                    
                     model = (DefaultTableModel) jTable8.getModel();
                    model.addRow(new Object[] {txtname.getText(), txtcat.getText(), txtunit.getText(), txtqty.getText(), array [2], lblTime.getText(), addDate});
                    txtPAmnt1.setText(Integer.toString(sum.getSum(jTable8, 4)));
                        txtPAmnt3.setText(Integer.toString(sum.getSum(jTable8, 4)));
                        table.FillTable(jTable11, order, jTextField9);
            table.FillTable(jTable13, due, jTextField10);
                    clearSale1 ();
                    btnProcess.setEnabled(true);
                    
        } catch (NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnUpdateItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateItemActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        DefaultTableModel model = (DefaultTableModel)jTable8.getModel();
        int selectedRowIndex = jTable8.getSelectedRow();
        String dates = model.getValueAt(selectedRowIndex, 6).toString();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser7.getDate());
                 String addDates = date.format(jDateChooser5.getDate());
         array [0] = Integer.parseInt(txtqty.getText());
        array [1] = Integer.parseInt(txttotal.getText());
        array [2] = (array [0] * array [1]);
        try {
         String query = "UPDATE sales SET Name = ?, Category = ?, Unit = ?, Quantity = ?, Selling_Price = ?, Total = ?, Mode_of_Payment = ? WHERE Time = ? AND Purchase_Date = ?";
                ps = con.prepareStatement(query);
                ps.setString(1, txtname.getText());
                ps.setString(2, txtcat.getText());
                ps.setString(3, txtunit.getText());
                ps.setInt(4, Integer.parseInt(txtqty.getText()));
                ps.setInt(5, Integer.parseInt(txttotal.getText()));
                ps.setInt(6, array [2]);
                ps.setObject(7, jComboBox1.getSelectedItem());
                ps.setString(8, txtPDate4.getText());
                ps.setString(9, dates);
                ps.executeUpdate();
                
                String quer = "UPDATE orders SET Customer = ?, Contact = ?, Address = ?, Name = ?, Category = ?, Unit = ?, Quantity = ?, Total = ?, Delivery_Date = ? WHERE Time = ? AND Date = ?";
                ps = con.prepareStatement(quer);
                ps.setString(1, txtName1.getText());
                ps.setString(2, txtContact.getText());
                ps.setString(3, txtAddress.getText());
                ps.setString(4, txtname.getText());
                ps.setString(5, txtcat.getText());
                ps.setString(6, txtunit.getText());
                ps.setInt(7, Integer.parseInt(txtqty.getText()));
                ps.setInt(8, array [2]);
                ps.setString(9, addDates);
                ps.setString(10, txtPDate4.getText());
                ps.setString(9, dates);
                ps.executeUpdate();
                
                model = (DefaultTableModel) jTable8.getModel();
                    int i = jTable8.getSelectedRow();
                    if (i >= 0) {
                        model.setValueAt(txtname.getText(), i, 0);
                        model.setValueAt(txtcat.getText(), i, 1);
                        model.setValueAt(txtunit.getText(), i, 2);
                        model.setValueAt(txtqty.getText(), i, 3);
                        model.setValueAt(array [2], i, 4);
                        model.setValueAt(txtPDate4.getText(), i, 5);
                        model.setValueAt(date, i, 6);

                         txtPAmnt1.setText(Integer.toString(sum.getSum(jTable8, 4)));
                        txtPAmnt3.setText(Integer.toString(sum.getSum(jTable8, 4)));
                    }
                    table.FillTable(jTable11, order, jTextField9);
            table.FillTable(jTable13, due, jTextField10);
                clearSale1 ();
                
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnUpdateItemActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        // TODO add your handling code here:
        array [0] = Integer.parseInt(txtqty.getText());
        array [1] = Integer.parseInt(txttotal.getText());
        array [2] = (array [0] * array [1]);
        Connection con = getConnection();
        DefaultTableModel model = (DefaultTableModel)jTable8.getModel();
        int selectedRowIndex = jTable8.getSelectedRow();
        String dates = model.getValueAt(selectedRowIndex, 6).toString();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser7.getDate());
        if (model.getRowCount() == 0) {
            btnAdd.setEnabled(true);
        }else {
            try {
                ps = con.prepareStatement("DELETE FROM sales WHERE Time = ? AND Purchase_Date = ?");
                ps.setString(1, txtPDate4.getText());
                ps.setString(2, dates);
                ps.executeUpdate();
                
                 ps = con.prepareStatement("DELETE FROM orders WHERE Time = ? AND Date = ?");
                ps.setString(1, txtPDate4.getText());
                ps.setString(2, dates);
                ps.executeUpdate();
                
                 int viewIndex = jTable8.getSelectedRow();
                if (viewIndex != -1) {
                    int modelIndex = jTable8.convertRowIndexToModel(viewIndex);
                    model = (DefaultTableModel)jTable8.getModel();
                    model.removeRow(modelIndex);
                    txtPAmnt1.setText(Integer.toString(sum.getSum(jTable8, 4)));
                    txtPAmnt3.setText(Integer.toString(sum.getSum(jTable8, 4)));
                    table.FillTable(jTable11, order, jTextField9);
            table.FillTable(jTable13, due, jTextField10);
                }
                clearSale1 ();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        // TODO add your handling code here:
        OrderPayment.setLocationRelativeTo(null);
        OrderPayment.setVisible(true);
    }//GEN-LAST:event_btnProcessActionPerformed

    private void txtname1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtname1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname1KeyReleased

    private void btnAdd1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdd1ActionPerformed
        // TODO add your handling code here:
        if (txtname1.getText().equals("") || txtcat1.getText().equals("") || txtunit1.getText().equals("") || txtqty1.getText().equals("") || txttotal1.getText().equals("")) {
        JOptionPane.showMessageDialog(null, "Some fields are empty");
        } else  {
            Connection con = getConnection();
        array [0] = Integer.parseInt(txtqty1.getText());
        array [1] = Integer.parseInt(txttotal1.getText());
        array [2] = (array [0] * array [1]);
        
        try {
            String x= txtcat1.getText();
        ps = con.prepareStatement("INSERT INTO sales (`Purchase_Date`, Time, `Name`, `Category`, `Unit`, `Quantity`, `Selling_Price`, `Total`, `Mode_Of_Payment`)" + " values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
                   SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser6.getDate());
                ps.setString(1, addDate);
                    ps.setString (2, lblTime.getText());
                    ps.setString (3, txtname1.getText());
                    ps.setString (4, x);
                    ps.setString (5, txtunit1.getText());
                    ps.setInt (6, Integer.parseInt(txtqty1.getText()));
                    ps.setInt (7, Integer.parseInt(txttotal1.getText()));
                    ps.setInt(8, array [2]);
                    ps.setObject(9, jComboBox2.getSelectedItem());
                    ps.executeUpdate();
                    
                     model = (DefaultTableModel) jTable9.getModel();
                    model.addRow(new Object[] {lblTime.getText(), txtname1.getText(), txtcat1.getText(), txtunit1.getText(), txtqty1.getText(), array [2], addDate});
                    txtPAmnt2.setText(Integer.toString(sum.getSum(jTable9, 5)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable9, 5)));
                    clearSale ();
                    btnProcess1.setEnabled(true);
        } catch (NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        }
    }//GEN-LAST:event_btnAdd1ActionPerformed

    private void btnUpdateItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateItem1ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        model = (DefaultTableModel) jTable9.getModel();
        int selectedRowIndex = jTable9.getSelectedRow();
        String dates = model.getValueAt(selectedRowIndex, 0).toString();
         array [0] = Integer.parseInt(txtqty1.getText());
        array [1] = Integer.parseInt(txttotal1.getText());
        array [2] = (array [0] * array [1]);
        try {
         String query = "UPDATE sales SET Name = ?, Category = ?, Unit = ?, Quantity = ?, Selling_Price = ?, Total = ?, Mode_of_Payment = ? WHERE Time = ? AND Purchase_Date = ?";
                ps = con.prepareStatement(query);
                ps.setString(1, txtname1.getText());
                ps.setString(2, txtcat1.getText());
                ps.setString(3, txtunit1.getText());
                ps.setInt(4, Integer.parseInt(txtqty1.getText()));
                ps.setInt(5, Integer.parseInt(txttotal1.getText()));
                ps.setInt(6, array [2]);
                ps.setObject(7, jComboBox2.getSelectedItem());
                ps.setString(8, txtPDate2.getText());
                ps.setString(9, dates);
                ps.executeUpdate();
                
                model = (DefaultTableModel) jTable9.getModel();
                    int i = jTable9.getSelectedRow();
                    if (i >= 0) {
                        model.setValueAt(txtPDate2.getText(), i, 0);
                        model.setValueAt(txtname1.getText(), i, 1);
                        model.setValueAt(txtcat1.getText(), i, 2);
                        model.setValueAt(txtunit1.getText(), i, 3);
                        model.setValueAt(txtqty1.getText(), i, 4);
                        model.setValueAt(array [2], i, 5);
                        model.setValueAt(dates, i, 6);

                        txtPAmnt2.setText(Integer.toString(sum.getSum(jTable9, 5)));
                        txtPAmnt.setText(Integer.toString(sum.getSum(jTable9, 5)));
                    }
                btnAdd1.setEnabled(true);
                btnUpdateItem1.setEnabled(false);
                btnRemove1.setEnabled(false);
                clearSale ();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnUpdateItem1ActionPerformed

    private void btnRemove1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemove1ActionPerformed
        // TODO add your handling code here:
        array [0] = Integer.parseInt(txtqty1.getText());
        array [1] = Integer.parseInt(txttotal1.getText());
        array [2] = (array [0] * array [1]);
        Connection con = getConnection();
        model = (DefaultTableModel) jTable9.getModel();
        int selectedRowIndex = jTable9.getSelectedRow();
        String dates = model.getValueAt(selectedRowIndex, 0).toString();
    DefaultTableModel model = (DefaultTableModel) jTable9.getModel();
        if (model.getRowCount() == 0) {
            btnAdd1.setEnabled(true);
        }else {
            try {
                ps = con.prepareStatement("DELETE FROM sales WHERE Time = ? AND Purchase_Date = ?");
                ps.setString(1, txtPDate2.getText());
                 SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser6.getDate());
                ps.setString(2, dates);
                ps.executeUpdate();
                
                 int viewIndex = jTable9.getSelectedRow();
                if (viewIndex != -1) {
                    int modelIndex = jTable9.convertRowIndexToModel(viewIndex);
                    model = (DefaultTableModel)jTable9.getModel();
                    model.removeRow(modelIndex);
                    txtPAmnt2.setText(Integer.toString(sum.getSum(jTable9, 5)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable9, 5)));
                    
                   btnAdd1.setEnabled(true);
                btnUpdateItem1.setEnabled(false);
                btnRemove1.setEnabled(false);
                    clearSale ();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnRemove1ActionPerformed

    private void btnProcess1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcess1ActionPerformed
        // TODO add your handling code here:
        SalePayment.setLocationRelativeTo(null);
        SalePayment.setVisible(true);
    }//GEN-LAST:event_btnProcess1ActionPerformed

    private void txtnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtnameCaretUpdate
        // TODO add your handling code here:
         Connection con = getConnection();
        try {
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM item_manager WHERE Name = ? AND Unit_of_Measurement = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtname.getText());
            ps.setString(2, txtcat.getText());

            rs = ps.executeQuery();
            if (rs.next()) {
                String cat = rs.getString("Category");
                txtcat.setText(cat);
                String unit = rs.getString("Unit_of_Measurement");
                txtunit.setText(unit);
                int prices = rs.getInt("Price");
                String price = String.valueOf(prices);
                txttotal.setText(price);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_txtnameCaretUpdate

    private void btnSearch2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch2ActionPerformed
        // TODO add your handling code here:
          table.FillTable(jTable12, items, jTextField11);
         OrderItems.setLocationRelativeTo(null);
        OrderItems.setVisible(true);
    }//GEN-LAST:event_btnSearch2ActionPerformed

    private void txtnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnameActionPerformed

    private void btnSearch3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch3ActionPerformed
        // TODO add your handling code here:
        table.FillTable(jTable10, items, jTextField8);
         SaleItems.setLocationRelativeTo(null);
        SaleItems.setVisible(true);
    }//GEN-LAST:event_btnSearch3ActionPerformed

    private void txtname1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtname1CaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname1CaretUpdate

    private void jTable10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable10MouseClicked
        // TODO add your handling code here:
         int index = jTable10.getSelectedRow();
        TableModel models = jTable10.getModel();
        String name = models.getValueAt(index, 0).toString();
        String cat = models.getValueAt(index, 1).toString();
        String unit = models.getValueAt(index, 2).toString();
        txtname1.setText(name);
        txtcat1.setText(cat);
        txtunit1.setText(unit);
        
         Connection con = getConnection();
        try {
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM item_manager WHERE Name = ? AND Unit_of_Measurement = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtname1.getText());
            ps.setString(2, txtunit1.getText());
            rs = ps.executeQuery();
            if (rs.next()) {
                String cate = rs.getString("Category");
                txtcat1.setText(cate);
                String prices = rs.getString("Price");
                txttotal1.setText(prices);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
            SaleItems.dispose();
        }
    }//GEN-LAST:event_jTable10MouseClicked

    private void jTextField8CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField8CaretUpdate
        // TODO add your handling code here:
         table.FillTable(jTable10, items, jTextField8);
    }//GEN-LAST:event_jTextField8CaretUpdate

    private void txtTenderedCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtTenderedCaretUpdate
        // TODO add your handling code here:
        try {
        int a = Integer.parseInt(txtPAmnt.getText());
        int b = Integer.parseInt(txtTendered.getText());
        int c = (a - b);
        int bal = c;
        String balance = String.valueOf(bal);
        txtBal.setText(balance);
        } catch (NumberFormatException n) {
        }
    }//GEN-LAST:event_txtTenderedCaretUpdate

    private void txtTendered1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtTendered1CaretUpdate
        // TODO add your handling code here:
        try {
            txtPAmnt3.setText(null);
        txtBal1.setText(null);
        txtPAmnt3.setText(txtPAmnt1.getText());
        int a = Integer.parseInt(txtPAmnt3.getText().trim());
        int b = Integer.parseInt(txtTendered1.getText().trim());
        int c = (a - b);
        int bal = c;
        String balance = String.valueOf(bal);
        txtBal1.setText(balance);
        } catch (NumberFormatException e) {
        }
    }//GEN-LAST:event_txtTendered1CaretUpdate

    private void btnSubmit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmit1ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        int a = Integer.parseInt(txtPAmnt3.getText());
        int b = Integer.parseInt(txtTendered1.getText());
        int c = (a - b);
        int bal = c;
        String balance = String.valueOf(bal);
        txtBal1.setText(balance);
        try {
             ps = con.prepareStatement("SELECT * FROM customers WHERE Customer = ?");
                ps.setString(1, txtName1.getText());
                rs = ps.executeQuery();
                if(rs.next()){
                    String names = txtName1.getText();
                    names = rs.getString(1);
                   String other = txtContact.getText();
                    other = rs.getString(1);
                } else {
                 ps = con.prepareStatement("INSERT INTO customers (Customer, Contact, Address)" + "values (?, ?, ?)");
                        ps.setString(1, txtName1.getText());
                        ps.setString(2, txtContact.getText());
                        ps.setString(3, txtAddress.getText());
                        ps.executeUpdate();
                }
                
                ps = con.prepareStatement("SELECT * FROM total_balance WHERE Customer = ?");
                ps.setString(1, txtName1.getText());
                rs = ps.executeQuery();
                if(rs.next()){
                    ttl = rs.getInt ("Grand_Total");
                    String names = txtName1.getText();
                    names = rs.getString(1);
                   String other = txtContact.getText();
                    other = rs.getString(1);

                    int total = Integer.parseInt(txtBal1.getText());
                    grand = (ttl + total);
                    String query = "UPDATE total_balance SET Grand_Total = ? WHERE Customer = ?";
                    ps = con.prepareStatement(query);
                ps.setInt(1, grand);
                ps.setString(2, txtName1.getText());
                 ps.executeUpdate();
                } else {
                 ps = con.prepareStatement("INSERT INTO total_balance (`Customer`, `Contact`, `Grand_Total`)" + "values (?, ?, ?)");
                        ps.setString(1, txtName1.getText());
                        ps.setString(2, txtContact.getText());
                        ps.setString(3, txtBal1.getText());
                        ps.executeUpdate();
                }
            
        int rowCount = model.getRowCount();
//Remove rows one by one from the end of the table
for (int i = rowCount - 1; i >= 0; i --) {
    model.removeRow(i);
}
txtPAmnt3.setText("0");
txtBal1.setText("0");
txtTendered1.setText("0");
txtPAmnt1.setText("0");
txtName1.setText(null);
txtContact.setText(null);
txtAddress.setText(null);
jDateChooser5.setDate(null);
clearSale ();

JOptionPane.showMessageDialog(null, "Order Saved");
OrderPayment.dispose();

        } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
            if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }

        }
    }//GEN-LAST:event_btnSubmit1ActionPerformed

    private void txtname2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtname2CaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname2CaretUpdate

    private void txtname2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtname2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname2ActionPerformed

    private void txtname2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtname2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname2KeyReleased

    private void txtName2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtName2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtName2ActionPerformed

    private void txtname3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtname3CaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname3CaretUpdate

    private void txtname3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtname3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname3ActionPerformed

    private void txtname3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtname3KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname3KeyReleased

    private void txtName3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtName3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtName3ActionPerformed

    private void btnRemove5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemove5ActionPerformed
        // TODO add your handling code here:
//                 ps = con.prepareStatement("DELETE FROM orders WHERE Order_ID = ?");
//                ps.setString(1, txtID2.getText());
//                ps.executeUpdate();
                
                 int viewIndex = jTable13.getSelectedRow();
                if (viewIndex != -1) {
                    int modelIndex = jTable13.convertRowIndexToModel(viewIndex);
                    model = (DefaultTableModel)jTable13.getModel();
                    model.removeRow(modelIndex);
                }
                
                  table.FillTable(jTable11, order, jTextField9);
                  txtID2.setText(null);
                  txtName3.setText(null);
                  txtContact2.setText(null);
                  txtAddress2.setText(null);
                  txtname3.setText(null);
                  txtcat3.setText(null);
                  txtunit3.setText("0");
                  txtqty3.setText("0");
                  txtPDate3.setText(null);
    }//GEN-LAST:event_btnRemove5ActionPerformed

    private void btnRemove6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemove6ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
         try { 
                 ps = con.prepareStatement("DELETE FROM orders WHERE Order_ID = ?");
                ps.setString(1, txtID.getText());
                ps.executeUpdate();
                
                 int viewIndex = jTable11.getSelectedRow();
                if (viewIndex != -1) {
                    int modelIndex = jTable11.convertRowIndexToModel(viewIndex);
                    model = (DefaultTableModel)jTable11.getModel();
                    model.removeRow(modelIndex);
                }
                
                 table.FillTable(jTable13, order, jTextField10);
                  txtID.setText(null);
                  txtName2.setText(null);
                  txtContact1.setText(null);
                  txtAddress1.setText(null);
                  txtname2.setText(null);
                  txtcat2.setText(null);
                  txtunit2.setText("0");
                  txtqty2.setText("0");
                  txtPDate5.setText(null);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
    }//GEN-LAST:event_btnRemove6ActionPerformed

    private void jTextField9CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField9CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable11, order, jTextField9);
    }//GEN-LAST:event_jTextField9CaretUpdate

    private void jTextField10CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField10CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable13, due, jTextField10);
//         table.FillTable(jTable13, order, jTextField10);
    }//GEN-LAST:event_jTextField10CaretUpdate

    private void jTable13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable13MouseClicked
        // TODO add your handling code here:
        int index = jTable13.getSelectedRow();
        TableModel models = jTable13.getModel();
        String ID = models.getValueAt(index, 0).toString();
        String customer = models.getValueAt(index, 2).toString();
        String item1 = models.getValueAt(index, 3).toString();
        txtID2.setText(ID);
        txtName3.setText(customer);
        txtname3.setText(item1);
        
         Connection con = getConnection();
        try {
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM orders WHERE Customer = ? AND Order_ID = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtName3.getText());
            ps.setString(2, txtID2.getText());

            rs = ps.executeQuery();
            if (rs.next()) {
                String contact = rs.getString("Contact");
                txtContact2.setText(contact);
                String address = rs.getString("Address");
                txtAddress2.setText(address);
                String cat = rs.getString("Category");
                txtcat3.setText(cat);
                String unit = rs.getString("Unit_of_Measurement");
                txtunit3.setText(unit);
                 int pri = rs.getInt("Quantity");
                String pric = String.valueOf(pri);
                txtqty3.setText(pric);
                 String dates = rs.getString("Delivery_Date");
                txtPDate3.setText(dates);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_jTable13MouseClicked

    private void jTable11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable11MouseClicked
        // TODO add your handling code here:
        int index = jTable11.getSelectedRow();
        TableModel models = jTable11.getModel();
        String ID = models.getValueAt(index, 0).toString();
        String customer = models.getValueAt(index, 2).toString();
        String item1 = models.getValueAt(index, 3).toString();
        txtID.setText(ID);
        txtName2.setText(customer);
        txtname2.setText(item1);
        
          Connection con = getConnection();
        try {
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM orders WHERE Customer = ? AND Order_ID = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtName2.getText());
            ps.setString(2, txtID.getText());

            rs = ps.executeQuery();
            if (rs.next()) {
                String contact = rs.getString("Contact");
                txtContact1.setText(contact);
                String address = rs.getString("Address");
                txtAddress1.setText(address);
                String cat = rs.getString("Category");
                txtcat2.setText(cat);
                String unit = rs.getString("Unit_of_Measurement");
                txtunit2.setText(unit);
                 int pri = rs.getInt("Quantity");
                String pric = String.valueOf(pri);
                txtqty2.setText(pric);
                 String dates = rs.getString("Delivery_Date");
                txtPDate5.setText(dates);
            } 
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_jTable11MouseClicked

    private void txtname1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtname1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtname1ActionPerformed

    private void txttotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal1ActionPerformed

    private void jTable9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable9MouseClicked
        // TODO add your handling code here:
        btnAdd1.setEnabled(false);
        btnUpdateItem1.setEnabled(true);
    btnRemove1.setEnabled(true);
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser6.getDate());
    
        model = (DefaultTableModel) jTable9.getModel();
        int selectedRowIndex = jTable9.getSelectedRow();
        txtPDate2.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtname1.setText(model.getValueAt(selectedRowIndex, 1).toString());
        txtcat1.setText(model.getValueAt(selectedRowIndex, 2).toString());
        txtunit1.setText(model.getValueAt(selectedRowIndex, 3).toString());
        txtqty1.setText(model.getValueAt(selectedRowIndex, 4).toString());
        Connection con = getConnection();
        try {
            String dates = model.getValueAt(selectedRowIndex, 6).toString();
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "select * from sales  WHERE Name = ? AND Time = ? AND Unit = ? AND Purchase_Date = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtname1.getText());
            ps.setString(2, txtPDate2.getText());
            ps.setString(3, txtunit1.getText());
            ps.setString(4, dates);

            rs = ps.executeQuery();
            if (rs.next()) {
                String SP = rs.getString("Selling_Price");
                txttotal1.setText(SP);
                String pay = rs.getString("Mode_of_Payment");
                jComboBox2.setSelectedItem(pay);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_jTable9MouseClicked

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
        try {
        int rowCount = model.getRowCount();
//Remove rows one by one from the end of the table
for (int i = rowCount - 1; i >= 0; i --) {
    model.removeRow(i);
}
clearSale ();
txtPAmnt.setText("0");
txtBal.setText("0");
txtTendered.setText("0");
txtPAmnt2.setText("0");
        } finally {
SalePayment.dispose();
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void jTable12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable12MouseClicked
        // TODO add your handling code here:
        int index = jTable12.getSelectedRow();
        TableModel models = jTable12.getModel();
        String name = models.getValueAt(index, 0).toString();
        String cat = models.getValueAt(index, 1).toString();
        String unit = models.getValueAt(index, 2).toString();
        txtname.setText(name);
        txtcat.setText(cat);
        txtunit.setText(unit);
        
         Connection con = getConnection();
        try {
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM item_manager WHERE Name = ? AND Unit_of_Measurement = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtname.getText());
            ps.setString(2, txtunit.getText());
            rs = ps.executeQuery();
            if (rs.next()) {
                String cate = rs.getString("Category");
                txtcat.setText(cate);
                String prices = rs.getString("Price");
                txttotal.setText(prices);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
            OrderItems.dispose();
        }
    }//GEN-LAST:event_jTable12MouseClicked

    private void jTextField11CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField11CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable12, items, jTextField11);
    }//GEN-LAST:event_jTextField11CaretUpdate

    private void txtTendered2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtTendered2CaretUpdate
        // TODO add your handling code here:
        try {
        int a = Integer.parseInt(txtPAmnt4.getText());
        int b = Integer.parseInt(txtTendered2.getText());
        int debt = (a - b);
        String balance = String.valueOf(debt);
        txtBal2.setText(balance);
        } catch (NumberFormatException n) {
        }
    }//GEN-LAST:event_txtTendered2CaretUpdate

    private void btnSubmit2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmit2ActionPerformed
        int a = Integer.parseInt(txtPAmnt4.getText());
        int b = Integer.parseInt(txtTendered2.getText());
        int debt = (a - b);
        String balance = String.valueOf(debt);
        txtBal2.setText(balance);
        Connection con = getConnection();
        String name = txtName4.getText();
        try {
           int row = jTable14.getSelectedRow();
            String debts = (jTable14).getModel().getValueAt(row, 0).toString();
            if (debt > 0) {
                con.setAutoCommit(false);
        String squery = "UPDATE `total_balance` SET Grand_Total = ? WHERE Customer = ?";
         ps = con.prepareStatement(squery);
         ps.setInt(1, debt);
         ps.setString(2, name);
         con.commit();
         ps.executeUpdate();
         con.commit();
            
            } else {
                String del = "DELETE FROM `total_balance` WHERE Customer = ?";
                PreparedStatement stmt = con.prepareStatement(del);
                stmt.setString(1, name);
                stmt.executeUpdate();
            }
         JOptionPane.showMessageDialog(null, "Record Update Successful");
         table.FillTable(jTable14, ttle, jTextField12);
         txtTendered2.setText(null);
         txtBal2.setText(null);
         txtName4.setText(null);
         txtContact3.setText(null);
                  txttotal3.setText("0");
                  
          } catch (SQLException ex) {
         JOptionPane.showMessageDialog(null, ex);
        } finally {
        if (rs != null) {
                        try {
                            rs.close();
                        } catch (SQLException e) { /* ignored */}
                    }
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) { /* ignored */}
                    }
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) { /* ignored */}
                    }
              MakePayment.dispose ();      
        }
    }//GEN-LAST:event_btnSubmit2ActionPerformed

    private void jTable8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable8MouseClicked
        // TODO add your handling code here:
        Connection con = getConnection();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser7.getDate());
        model = (DefaultTableModel)jTable8.getModel();

        // get the selected row index
        int selectedRowIndex = jTable8.getSelectedRow();

        // set the selected row data into jtextfields
        txtname.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtcat.setText(model.getValueAt(selectedRowIndex, 1).toString());
        txtunit.setText(model.getValueAt(selectedRowIndex, 2).toString());
        txtqty.setText(model.getValueAt(selectedRowIndex, 3).toString());
        txtPDate4.setText(model.getValueAt(selectedRowIndex, 5).toString());
        try {
        String dates = model.getValueAt(selectedRowIndex, 6).toString();
         Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM sales WHERE Time = ? AND Name = ? AND Unit = ? AND Purchase_Date = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, txtPDate4.getText());
            ps.setString(2, txtname.getText());
            ps.setString(3, txtunit.getText());
            ps.setString(4, dates);
            rs = ps.executeQuery();
            if (rs.next()) {
                String units = rs.getString("Selling_Price");
                txtcat.setText(units);
            }
        } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jTable8MouseClicked

    private void jTable14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable14MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)jTable14.getModel();

        // get the selected row index
        int selectedRowIndex = jTable14.getSelectedRow();

        // set the selected row data into jtextfields
        txtName4.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtContact3.setText(model.getValueAt(selectedRowIndex, 1).toString());
        txttotal3.setText(model.getValueAt(selectedRowIndex, 2).toString());
        txtPAmnt4.setText(model.getValueAt(selectedRowIndex, 2).toString());
    }//GEN-LAST:event_jTable14MouseClicked

    private void jTextField12CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField12CaretUpdate
        // TODO add your handling code here:
        table.FillTable(jTable14, ttle, jTextField12);
    }//GEN-LAST:event_jTextField12CaretUpdate

    private void txtName4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtName4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtName4ActionPerformed

    private void btnRemove8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemove8ActionPerformed
        // TODO add your handling code here:
        MakePayment.setLocationRelativeTo(null);
        MakePayment.setVisible(true);
        
    }//GEN-LAST:event_btnRemove8ActionPerformed

    private void txtContact3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContact3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtContact3ActionPerformed

    private void btnNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNew1ActionPerformed
        // TODO add your handling code here:
        txtFName2.setText(null);
        txtFName5.setText(null);
        btnSave5.setEnabled(true);
        btnDel3.setEnabled(false);
    }//GEN-LAST:event_btnNew1ActionPerformed

    private void btnSave5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave5ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
       if (txtFName2.getText().equals("") || txtFName5.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some Fields are Empty!");
        } else {
            try {
                        ps = con.prepareStatement("INSERT INTO `user`(`First_Name`, `Password`)" + " values(?, ?)");
                        ps.setString (1, txtFName2.getText());
                        ps.setObject(2, txtFName5.getText());
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record saved");
                        table1.FillTable(jTable3, user);
                        txtFName2.setText(null);
        txtFName5.setText(null);
        btnDel3.setEnabled(true);
                          } catch (HeadlessException | NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnSave5ActionPerformed

    private void btnDel3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel3ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        try {
            ps = con.prepareStatement("DELETE FROM `user` WHERE `First_Name` = ? AND `Password` = ?");
            String name = txtFName2.getText();
            ps.setString(1, name);
            String pass = String.valueOf(txtFName5.getText());
            ps.setString(2,pass);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record Deleted");
            table1.FillTable(jTable3, user);
            btnSave5.setEnabled(true);
            txtFName2.setText(null);
        txtFName5.setText(null);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnDel3ActionPerformed

    private void btnDel4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel4ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        try {
            ps = con.prepareStatement("DELETE FROM `administrator` WHERE `Admin_Name` = ? AND `Admin_Password` = ?");
            String name = txtFName3.getText();
            ps.setString(1, name);
            String pass = String.valueOf(txtFName4.getText());
            ps.setString(2,pass);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record Deleted");
            table1.FillTable(jTable4, admin);
            btnSave6.setEnabled(true);
            txtFName3.setText(null);
        txtFName4.setText(null);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }//GEN-LAST:event_btnDel4ActionPerformed

    private void btnSave6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave6ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
       if (txtFName3.getText().equals("") || txtFName4.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some Fields are Empty!");
        } else {
            try {
                        ps = con.prepareStatement("INSERT INTO `administrator`(`Admin_Name`, `Admin_Password`)" + " values(?, ?)");
                        ps.setString (1, txtFName3.getText());
                        ps.setObject(2, txtFName4.getText());
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record saved");
                        table1.FillTable(jTable4, admin);
                        txtFName3.setText(null);
        txtFName4.setText(null);
        btnDel4.setEnabled(true);
                          } catch (HeadlessException | NumberFormatException | SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) { /* ignored */}
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) { /* ignored */}
                }
            }
        }
    }//GEN-LAST:event_btnSave6ActionPerformed

    private void btnNew2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNew2ActionPerformed
        // TODO add your handling code here:
        txtFName3.setText(null);
        txtFName4.setText(null);
        btnSave6.setEnabled(true);
        btnDel4.setEnabled(false);
    }//GEN-LAST:event_btnNew2ActionPerformed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        btnSave5.setEnabled(false);
        btnDel3.setEnabled(true);
    
    model = (DefaultTableModel) jTable3.getModel();
        int selectedRowIndex = jTable3.getSelectedRow();

        txtFName2.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtFName5.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable3MouseClicked

    private void jTable4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable4MouseClicked
        // TODO add your handling code here:
        btnSave6.setEnabled(false);
        btnDel4.setEnabled(true);
    
    model = (DefaultTableModel) jTable4.getModel();
        int selectedRowIndex = jTable4.getSelectedRow();

        txtFName3.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtFName4.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable4MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
             for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
             UIManager.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main_Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog Accounts;
    private javax.swing.JDialog Calculator;
    private javax.swing.JDialog Change_Pass;
    private javax.swing.JDialog Expenses;
    private javax.swing.JDialog MakePayment;
    private javax.swing.JDialog Menus;
    private javax.swing.JDialog NewSale;
    private javax.swing.JDialog Order;
    private javax.swing.JDialog OrderCustomer;
    private javax.swing.JDialog OrderItems;
    private javax.swing.JDialog OrderPayment;
    private javax.swing.JDialog Report;
    private javax.swing.JDialog SaleItems;
    private javax.swing.JDialog SalePayment;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAdd1;
    private javax.swing.JButton btnCancel1;
    private javax.swing.JButton btnDel1;
    private javax.swing.JButton btnDel2;
    private javax.swing.JButton btnDel3;
    private javax.swing.JButton btnDel4;
    private javax.swing.JButton btnDelete1;
    private javax.swing.JButton btnExpense;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnMenus;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnNew1;
    private javax.swing.JButton btnNew2;
    private javax.swing.JButton btnNewSale;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnPassword;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnProcess1;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnRemove1;
    private javax.swing.JButton btnRemove5;
    private javax.swing.JButton btnRemove6;
    private javax.swing.JButton btnRemove8;
    private javax.swing.JButton btnReport;
    private javax.swing.JButton btnSave2;
    private javax.swing.JButton btnSave3;
    private javax.swing.JButton btnSave4;
    private javax.swing.JButton btnSave5;
    private javax.swing.JButton btnSave6;
    private javax.swing.JButton btnSearch1;
    private javax.swing.JButton btnSearch2;
    private javax.swing.JButton btnSearch3;
    private javax.swing.JButton btnSubmit1;
    private javax.swing.JButton btnSubmit2;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdateItem;
    private javax.swing.JButton btnUpdateItem1;
    private javax.swing.JButton btnUpdatePass;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton31;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private com.toedter.calendar.JDateChooser jDateChooser4;
    private com.toedter.calendar.JDateChooser jDateChooser5;
    private com.toedter.calendar.JDateChooser jDateChooser6;
    private com.toedter.calendar.JDateChooser jDateChooser7;
    private com.toedter.calendar.JDateChooser jDateChooser8;
    private com.toedter.calendar.JDateChooser jDateChooser9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JTextField jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable10;
    private javax.swing.JTable jTable11;
    private javax.swing.JTable jTable12;
    private javax.swing.JTable jTable13;
    private javax.swing.JTable jTable14;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTable jTable7;
    private javax.swing.JTable jTable8;
    private javax.swing.JTable jTable9;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblFName2;
    private javax.swing.JLabel lblFName3;
    private javax.swing.JLabel lblPass3;
    private javax.swing.JLabel lblPass4;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotal1;
    private javax.swing.JLabel lblUser1;
    private javax.swing.JLabel lblUser10;
    private javax.swing.JLabel lblUser2;
    private javax.swing.JLabel lblUser4;
    private javax.swing.JLabel lblUser5;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtAddress1;
    private javax.swing.JTextField txtAddress2;
    private javax.swing.JTextField txtAmnt;
    private javax.swing.JLabel txtBal;
    private javax.swing.JLabel txtBal1;
    private javax.swing.JLabel txtBal2;
    private javax.swing.JComboBox<String> txtCat;
    private javax.swing.JTextField txtCat1;
    public javax.swing.JTextField txtContact;
    public javax.swing.JTextField txtContact1;
    public javax.swing.JTextField txtContact2;
    public javax.swing.JTextField txtContact3;
    private javax.swing.JTextField txtExpense;
    private javax.swing.JTextField txtFName2;
    private javax.swing.JTextField txtFName3;
    private javax.swing.JTextField txtFName4;
    private javax.swing.JTextField txtFName5;
    private javax.swing.JLabel txtID;
    private javax.swing.JTextField txtID1;
    private javax.swing.JLabel txtID2;
    private javax.swing.JTextArea txtInfo;
    private javax.swing.JTextField txtName;
    public javax.swing.JTextField txtName1;
    public javax.swing.JTextField txtName2;
    public javax.swing.JTextField txtName3;
    public javax.swing.JTextField txtName4;
    private javax.swing.JLabel txtPAmnt;
    private javax.swing.JLabel txtPAmnt1;
    private javax.swing.JLabel txtPAmnt2;
    private javax.swing.JLabel txtPAmnt3;
    private javax.swing.JLabel txtPAmnt4;
    private javax.swing.JLabel txtPDate2;
    private javax.swing.JLabel txtPDate3;
    private javax.swing.JLabel txtPDate4;
    private javax.swing.JLabel txtPDate5;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtTendered;
    private javax.swing.JTextField txtTendered1;
    private javax.swing.JTextField txtTendered2;
    private javax.swing.JTextField txtUnit;
    private javax.swing.JTextField txtcat;
    private javax.swing.JTextField txtcat1;
    private javax.swing.JTextField txtcat2;
    private javax.swing.JTextField txtcat3;
    private javax.swing.JPasswordField txtconfirm;
    private javax.swing.JPasswordField txtcurr;
    private javax.swing.JTextField txtname;
    private javax.swing.JTextField txtname1;
    private javax.swing.JTextField txtname2;
    private javax.swing.JTextField txtname3;
    private javax.swing.JPasswordField txtnew;
    private javax.swing.JTextField txtqty;
    private javax.swing.JTextField txtqty1;
    private javax.swing.JTextField txtqty2;
    private javax.swing.JTextField txtqty3;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txttotal1;
    private javax.swing.JTextField txttotal3;
    private javax.swing.JTextField txttotal4;
    private javax.swing.JTextField txtunit;
    private javax.swing.JTextField txtunit1;
    private javax.swing.JTextField txtunit2;
    private javax.swing.JTextField txtunit3;
    // End of variables declaration//GEN-END:variables

    public void fillCombo () {
        Connection con = getConnection();
        txtCat.removeAllItems();
    try {
    String cbo = "SELECT * FROM menu_category";
    ps = con.prepareStatement(cbo);
    rs = ps.executeQuery();
    while (rs.next ()) {
    String name = rs.getString("Category");
    txtCat.addItem (name);
    }
    } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }
     
     public void clearSale () {
     txtname1.setText(null);
     txtcat1.setText(null);
     txtunit1.setText(null);
     txtqty1.setText("0");
     txttotal1.setText("0");
     jComboBox2.setSelectedIndex(0);
     }
     
     public void clearSale1 () {
     txtname.setText(null);
     txtcat.setText(null);
     txtunit.setText(null);
     txtqty.setText("0");
     txttotal.setText("0");
     jComboBox1.setSelectedIndex(0);
     }
}
